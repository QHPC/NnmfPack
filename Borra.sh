#!/bin/bash
lista1=`find . -name "*~" -print`
lista2="stamp-h1 aclocal.m4 config.h.in configure m4 autom4te.cache config config.h config.log config.status libtool example/exampleCpu/create_examples.sh example/exampleGpu/create_examples.sh interfaces/matlab/mexCpu/create_mex_files.sh \
.libs/ src/CPU/.libs/ src/CPU/NMF_General/.libs/ src/CPU/NMF_Specific/.libs/ src/CPU/NNLS_mrhs/.libs/ src/CPU/Utils_CPU/.libs/ src/GPU/.libs/ \
src/GPU/NMF_General/.deps/ src/GPU/NMF_General/.dirstamp src/GPU/NMF_General/.libs/ src/GPU/NMF_Specific/.deps/ src/GPU/NMF_Specific/.dirstamp \
src/GPU/NMF_Specific/.libs/ src/GPU/Utils_GPU/.deps/ src/GPU/Utils_GPU/.dirstamp src/GPU/Utils_GPU/.libs/"
lista3=`find . -name Makefile.in -print`
lista4=`find . -name Makefile -print`
lista5="interfaces/matlab/mexGpu/create_mex_files.sh libnnmfpack.la src/CPU/NMF_General/libfuncionesCPU_la-anlsbpp_cpu.lo \
src/CPU/NMF_General/libfuncionesCPU_la-anlsbpp_cpu.o src/CPU/NMF_General/libfuncionesCPU_la-fhals_cpu.lo src/CPU/NMF_General/libfuncionesCPU_la-fhals_cpu.o \
src/CPU/NMF_General/libfuncionesCPU_la-gcd_cpu.lo src/CPU/NMF_General/libfuncionesCPU_la-gcd_cpu.o src/CPU/NMF_General/libfuncionesCPU_la-mlsa_cpu.lo \
src/CPU/NMF_General/libfuncionesCPU_la-mlsa_cpu.o src/CPU/NMF_Specific/libfuncionesCPU_la-affine_cpu.lo src/CPU/NMF_Specific/libfuncionesCPU_la-affine_cpu.o \
src/CPU/NMF_Specific/libfuncionesCPU_la-bdiv_cpu.lo src/CPU/NMF_Specific/libfuncionesCPU_la-bdiv_cpu.o 
src/CPU/NNLS_mrhs/libfuncionesCPU_la-asna_cpu.lo src/CPU/NNLS_mrhs/libfuncionesCPU_la-asna_cpu.o src/CPU/NNLS_mrhs/libfuncionesCPU_la-bpp_cpu.lo \
src/CPU/NNLS_mrhs/libfuncionesCPU_la-bpp_cpu.o src/CPU/NNLS_mrhs/libfuncionesCPU_la-pbdiv_cpu.lo src/CPU/NNLS_mrhs/libfuncionesCPU_la-pbdiv_cpu.o \
src/CPU/Utils_CPU/libfuncionesCPU_la-utils_cpu.lo src/CPU/Utils_CPU/libfuncionesCPU_la-utils_cpu.o src/CPU/libfuncionesCPU.la \
src/GPU/NMF_General/fhals_cuda.lo src/GPU/NMF_General/fhals_cuda.o src/GPU/NMF_General/mlsa_cuda.lo src/GPU/NMF_General/mlsa_cuda.o \
src/GPU/NMF_Specific/bdiv_cuda.lo src/GPU/NMF_Specific/bdiv_cuda.o src/GPU/Utils_GPU/utils_cuda.lo src/GPU/Utils_GPU/utils_cuda.o \
src/GPU/libfuncionesGPU.la"

for i in $lista1; do
 rm -f $i
done
    
for i in $lista2; do
 rm -rf $i
done

for i in $lista3; do
 rm -f $i
done

for i in $lista4; do
 rm -f $i
done
                     
for i in $lista5; do
 rm -f $i
done