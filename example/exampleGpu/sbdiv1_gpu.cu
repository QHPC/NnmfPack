/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    sbdiv1_gpu.cu
 *  \brief   Beta Divergence (bdiv) example of usage. Matrices A, W and H are
 *           randomly generated. Simple precision is used. Layout 1D column-mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include <nnmfpackGpu.h>

int main(int argc, char *argv[])
{
  int
    m,      /* number of rows   of matrix A */
    n,      /* number of colums of matrix A */
    k,      /* number of colums of matrix W and number of rows of matrix H */
    uType,  /* What to update? W and H (uType=1), W (uType=2) or H (uType=3) */
    status,
    iters,
    devID,
    seed;

  float
    *d_A=NULL,   /* Inside device */
    *d_W=NULL,   /* Inside device */
    *d_H=NULL,   /* Inside device */	
    beta =2.0f,
    error=0.0f,
    Time =0.0f;

  cudaDeviceProp
    deviceProp;

  cudaEvent_t
    start, stop;

  if (argc != 8)
  {
    printf("Apply Beta-Divergence. A, W and H are randomly generated simple precision matrices.\n");
    printf("\tA: is an m x n matrix.\n");
    printf("\tW: is an m x k matrix.\n");
    printf("\tH: is an k x n matrix.\n");
    printf("\tbeta: beta value.\n");
    printf("\tuType: 1 for update W and H; 2 for update W only; 3 for update H only.\n");
    printf("\titer: number of iterations.\n");
    printf("\tseed: seed for the Pseudo Random Generator.\n");
    printf("Usage: %s <m> <n> <k> <beta> <uType> <iter> <seed>\n", argv[0]);
    return -1;
  }

  m    =atoi(argv[1]);
  n    =atoi(argv[2]);  
  k    =atoi(argv[3]);
  beta =(float)atof(argv[4]);
  uType=atoi(argv[5]);
  iters=atoi(argv[6]);
  seed =atoi(argv[7]);
  
  if ((m <= 0) || (n <= 0))
  {
    printf("Input matrix A incorrect: null or negative dimension\n");
    return -1;
  }

  if ((k > m) || (k > n))
  {
    printf("k must be less than min(n,m)\n");
    return -1;
  }
 
  CUDAERR(cudaGetDevice(&devID));
  CUDAERR(cudaGetDeviceProperties(&deviceProp, devID));
  printf("Using GPU %d (\"%s\") with compute capability %d.%d\n\n", devID, deviceProp.name, deviceProp.major, deviceProp.minor);

  CUDAERR(cudaMalloc((void **)&d_A, m * n * sizeof(float)));
  CUDAERR(cudaMalloc((void **)&d_W, m * k * sizeof(float)));
  CUDAERR(cudaMalloc((void **)&d_H, k * n * sizeof(float)));

  /* Choose random A, W, H */
  slarngenn_cuda(m, n, seed+11, d_A);
  slarngenn_cuda(m, k, seed+13, d_W);
  slarngenn_cuda(k, n, seed+17, d_H);

  CUDAERR(cudaEventCreate(&start));
  CUDAERR(cudaEventCreate(&stop));

  /* Calling beta-divergence cuda kernel */
  CUDAERR(cudaEventRecord(start, NULL));
    /* uType=1 --> Update W and H */
    /* uType=2 --> Only update W  */
    /* uType=3 --> Only update H  */    
    status = sbdiv_cuda(m, n, k, d_A, d_W, d_H, beta, uType, iters);
  CUDAERR(cudaEventRecord(stop, NULL));

  CUDAERR(cudaEventSynchronize(stop));
  CUDAERR(cudaEventElapsedTime(&Time, start, stop));

  if (status != 0)
  {
    printf("Problems with Beta-Divergence. Error code: %d\n", status);
    return -1;
  }

  /* Frobenious norm or Beta_divergence error based. */
  /* Choose one.                                     */
  /* error=serror_cuda(m, n, k, d_A, d_W, d_H);      */
  error=serrorbd_cuda(m, n, k, d_A, d_W, d_H, beta);

  printf("#  M    N     K      Beta       Iteration    Time (sec)      Error   Update Model\n");
  printf("# -----------------------------------------------------------------------------------\n");     
  printf("%4d  %4d  %4d    %1.6f    %4d        %1.5E    %1.8E    %1d\n", m, n, k, beta, iters, Time/1000.0f, error, uType);

  CUDAERR(cudaFree(d_A));
  CUDAERR(cudaFree(d_W));
  CUDAERR(cudaFree(d_H));

  CUDAERR(cudaDeviceReset());

  return 0;
}
