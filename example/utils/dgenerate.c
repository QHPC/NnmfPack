/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *   nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    dgenerate.c
 *  \brief   Auxiliar code to randomly generate matrices. Matrices are 
 *           written to a binary file. Double precision is used.
 *           Layout: 1D Column mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef With_MKL
  #include <mkl.h>
#else
  #include <cblas.h>
#endif

int main(int argc, char *argv[])
{
  FILE
    *F_Out;

  int 
    n=0, m=0, seed=2121, iseed[4], uno=1, size,
    distribucion_tipo=1,
    distribucion_elem=0;

  double
    *Matrix=NULL,
    scale=1.0;

  char
    *out=NULL;

  if ((argc < 5) || (argc > 6)) {
    printf("Generate (randomly uniform (0, 1)*scal) a double precision matrix.\n");
    printf("\tn:    number of rows    of the matriz\n");
    printf("\tm:    number of columns of the matriz\n");
    printf("\tscal: constant to scale matriz elements\n");
    printf("\tfile: name of the file to store the matriz\n");
    printf("\tseed: Initial seed value (optional)\n\n");
    printf("Usage: %s <n> <m> <scal> <file> [seed]\n", argv[0]);
    return 0;
  }
   
  n    =atoi(argv[1]);
  m    =atoi(argv[2]);
  scale=atof(argv[3]);
  out  =strdup(argv[4]);

  if(argc==6)
    seed=atoi(argv[5]);

  distribucion_elem=seed % 4095;

  if ((distribucion_elem % 2) == 0)
    distribucion_elem++;

  iseed[0]=iseed[1]=iseed[2]=iseed[3]=distribucion_elem;

  size=n*m;

  Matrix =(double *)malloc(size*sizeof(double));
  if (Matrix==NULL)
  {
    printf("Error: Can not allocate Matirx in memory\n");
    return -1;
  }   

  if ((F_Out=fopen(out, "w")) == NULL)
  {
    printf("Error: Can not open %s output file\n", out);
    return -1;
  }
   
  #ifdef With_MKL
    dlarnv(&distribucion_tipo, iseed, &size, Matrix);
  #else
    dlarnv_(&distribucion_tipo, iseed, &size, Matrix);
  #endif

  if (!(scale >= 1.0 && scale <=1.0))
  {
    #ifdef With_MKL
      dscal(&size, &scale, Matrix, &uno);
    #else
      dscal_(&size, &scale, Matrix, &uno);
    #endif
  }

  fwrite(&n, sizeof(int), 1, F_Out);
  fwrite(&m, sizeof(int), 1, F_Out); 
  fwrite(Matrix, size*sizeof(double), 1, F_Out);

  free(Matrix); Matrix=NULL;
   
  fclose(F_Out);

  printf("File size %.1f megabytes\n", size*sizeof(double)/(1024.0*1024.0));

  return 0;
}
