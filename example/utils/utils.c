/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    utils.c
 *  \brief   Some transversal auxiliar functions. Double and simple precision
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include <stdio.h>
#include <stdlib.h>
#include "utils.h"


/**
 *  \fn     int read_file_header(char *path, int *rows, int *cols)
 *  \brief  read_file_header fills rows & cols with the dimensions of a matrix stored
 *          in the binary file. It returns 0 if not problems were found.
 *  \param path:  (input) Path to the file
 *  \param rows: (output) Number of rows of the stored matrix in the file
 *  \param cols: (output) Number of columns of the stored matrix in the file
*/
int read_file_header(char *path, int *rows, int *cols) 
{
  FILE *fp;

  if ((fp = fopen(path, "r")) == NULL)
     return -1;

  fread(rows, sizeof(int), 1, fp);
  fread(cols, sizeof(int), 1, fp);

  fclose(fp);
  return 0;
}


/**
 *  \fn     int dread_file(char *path, int rows, int cols, double *M)
 *  \brief  dread_file fills the double precision matix M stored in the binary file.
 *          It returns 0 if not problems were found.
 *  \param path:  (input) Path to the file
 *  \param rows:  (input) Number of rows of the matrix M
 *  \param cols:  (input) Number of columns of the matrix M
 *  \param M:    (output) Double precision matrix M stored as 1D layout
*/
int dread_file(char *path, int rows, int cols, double *M) 
{
  FILE *fp;
  int tmp_rows = 0, 
      tmp_cols = 0;

  if ((fp = fopen(path, "r")) == NULL)
     return -1;

  fread(&tmp_rows, sizeof(int), 1, fp);
  fread(&tmp_cols, sizeof(int), 1, fp);

  if ((tmp_rows == rows) && (tmp_cols == cols))
  {
     fread(M, rows * cols * sizeof(double), 1, fp);
     fclose(fp);
     return 0;
  }
  else 
  {
     printf("Inconsistency between arguments and headers\n");
     fclose(fp);
     return -1;
  }
}


/**
 *  \fn     int sread_file(char *path, int rows, int cols, float *M)
 *  \brief  sread_file fills the simple precision matix M stored in the binary file.
 *          It returns 0 if not problems were found.
 *  \param path:  (input) Path to the file
 *  \param rows:  (input) Number of rows of the matrix M
 *  \param cols:  (input) Number of columns of the matrix M
 *  \param M:    (output) Simple precision matrix M stored as 1D layout
*/
int sread_file(char *path, int rows, int cols, float *M) 
{
  FILE *fp;
  int tmp_rows = 0, 
      tmp_cols = 0;

  if ((fp = fopen(path, "r")) == NULL)
     return -1;

  fread(&tmp_rows, sizeof(int), 1, fp);
  fread(&tmp_cols, sizeof(int), 1, fp);

  if ((tmp_rows == rows) && (tmp_cols == cols))
  {
     fread(M, rows * cols * sizeof(float), 1, fp);
     fclose(fp);
     return 0;
  }
  else 
  {
     printf("Inconsistency between arguments and headers\n");
     fclose(fp);
     return -1;
  }
}


/**
 *  \fn     int dwrite_file(char *path, int *rows, int *cols, double *M)
 *  \brief  dwrite_file stores the double precision matix M in the binary file.
 *          It returns 0 if not problems were found.
 *  \param path: (input) Path to the file
 *  \param rows: (input) Number of rows of the matrix M
 *  \param cols: (input) Number of columns of the matrix M
 *  \param M:    (input) Double precision matrix M stored as 1D layout
*/
int dwrite_file(char *path, int *rows, int *cols, double *M) 
{
  FILE *fp;

  if ((fp = fopen(path, "w")) == NULL)
     return -1;

  fwrite(rows, sizeof(int), 1, fp);
  fwrite(cols, sizeof(int), 1, fp);
  fwrite(M, ((*rows) * (*cols) * sizeof(double)), 1, fp);

  fclose(fp);
  return 0;
}


/**
 *  \fn     int swrite_file(char *path, int *rows, int *cols, float *M)
 *  \brief  swrite_file stores the simple precision matix M in the binary file.
 *          It returns 0 if not problems were found.
 *  \param path: (input) Path to the file
 *  \param rows: (input) Number of rows of the matrix M
 *  \param cols: (input) Number of columns of the matrix M
 *  \param M:    (input) Simple precision matrix M stored as 1D layout
*/
int swrite_file(char *path, int *rows, int *cols, float *M) 
{
  FILE *fp;

  if ((fp = fopen(path, "w")) == NULL)
     return -1;

  fwrite(rows, sizeof(int), 1, fp);
  fwrite(cols, sizeof(int), 1, fp);
  fwrite(M, ((*rows) * (*cols) * sizeof(float)), 1, fp);

  fclose(fp);
  return 0;
}


/**
 *  \fn     int dwrite_ascii_file(char *path, int rows, int cols, double *M)
 *  \brief  dwrite_ascii_file stores the double precision matix M in the text file.
 *          It returns 0 if not problems were found.
 *  \param path: (input) Path to the file
 *  \param rows: (input) Number of rows of the matrix M
 *  \param cols: (input) Number of columns of the matrix M
 *  \param M:    (input) Simple precision matrix M stored as 1D layout
*/
int dwrite_ascii_file(char *path, int rows, int cols, double *M) 
{
  FILE *fp;
  int i=0;

  if ((fp=fopen(path, "w")) == NULL)
     return -1;

  fprintf(fp, "%d  %d\n", rows, cols);

  for (i = 0; i < rows * cols; i++)
    fprintf(fp, "%13.10f\n", M[i]);

  fclose(fp);
  return 0;
} 


/**
 *  \fn     int swrite_ascii_file(char *path, int rows, int cols, float *M)
 *  \brief  swrite_ascii_file stores the simple precision matix M in the text file.
 *          It returns 0 if not problems were found.
 *  \param path: (input) Path to the file
 *  \param rows: (input) Number of rows of the matrix M
 *  \param cols: (input) Number of columns of the matrix M
 *  \param M:    (input) Simple precision matrix M stored as 1D lahout
*/
int swrite_ascii_file(char *path, int rows, int cols, float *M) 
{
  FILE *fp;
  int i=0;

  if ((fp=fopen(path, "w")) == NULL)
     return -1;

  fprintf(fp, "%d  %d\n", rows, cols);

  for (i = 0; i < rows * cols; i++)
    fprintf(fp, "%13.10f\n", M[i]);

  fclose(fp);
  return 0;
} 
