/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and INCO2 (Univesity *
 *   of Valencia) groups                                                   *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    mex_bdivCpu.c
 *  \brief   mex_bdivCpu is the nnmfpack's CPU-driver (nnmfpack's functions runs
 *           on CPU) for using it from from Matlab/Octave. Layout 1D column-mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/

#include <nnmfpackCpu.h>
#include <string.h>
#include "mex.h"

/**
 *  \fn    void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
 *  \brief Standar MEX interface. Double precision.
 *  \param nlhs: (output) Number of expected output positions of mxArrays with data
 *  \param plhs: (output) Outpur parameters. They are: W and H matrices
 *  \param nrhs: (input)  Number of input positions of mxArray with data
 *  \param prhs: (input)  Input parameters. They are: A, W and H matrices, beta value, uType and iterations
 *                        uType can be UpdateAll (W and H are updated), UpdateW (only H) or UpdateH (only W)
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int
    m, n, k, nIts;

  double
    *A=NULL, *W=NULL, *H=NULL, beta, *W0=NULL, *H0=NULL;

  if (nrhs==5 && nlhs==2)
  {
    /* mexPrintf("mex_bdivCpu: start...\n"); */
 
    /* Input parameters: mxGetN --> number of columns; mxGetM --> number of rows */
    m = mxGetM(prhs[0]);
    n = mxGetN(prhs[0]);
    k = mxGetN(prhs[1]);

    A  = mxGetPr(prhs[0]);
    W0 = mxGetPr(prhs[1]);
    H0 = mxGetPr(prhs[2]);

    beta = mxGetScalar(prhs[3]);
    nIts = (int)mxGetScalar(prhs[4]);

    /* Output parameters */
    plhs[0] = mxCreateDoubleMatrix(m,k, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(k,n, mxREAL);
  
    W = mxGetPr(plhs[0]);
    H = mxGetPr(plhs[1]);
  
    memcpy(W, W0, m*k*mxGetElementSize(prhs[1]));
    memcpy(H, H0, k*n*mxGetElementSize(prhs[2]));
 
    dbdiv_cpu(m, n, k, A, W, H, beta, nIts);

    /* mexPrintf("... end\n"); */
  }
  else
    mexPrintf("The number of inputs/expected-output positions of mex_bdivCpu are wrong\n"); 
} 
