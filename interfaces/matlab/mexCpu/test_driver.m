function test_driver(m, n, k, beta, numIters);
  %% A is a mxn matrix
  %% W is a mxk matrix
  %% H is a kxn matrix
  %% beta is the beta-divergencia value for beta
  %% uType=1 --> Update W and H
  %% uType=2 --> Only update W
  %% uType=3 --> Only update H
  %% numIters is the number of iterations
  
  A0=rand(m,n);
  W0=rand(m,k);
  H0=rand(k,n);

  tic();
    [W,H]=mex_bdivCpu(A0, W0, H0, beta, numIters);
  time=toc();

  disp(sprintf('NnmfPack with (m, n, k, beta, numIters, |A-WH|, Time)=(%d, %d, %d, %f, %d, %f, %f)\n', m, n, k, beta, numIters, norm(A0-W*H), time));

  exit;
