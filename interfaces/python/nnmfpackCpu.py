#!/usr/bin/env python

"""nnmfpackCPU.py: Public interface for the CPU functions of the NNMFPACK library."""

__author__ = "P. San Juan, Interdisciplinary Computation and Communication Group (INCO2), Universitat Politecnica de Valencia,Spain"
__date__ = "25/06/18"


import numpy as np                                                                                                                                                                               
import numpy.ctypeslib as npct                                                                                                                                                                   
from ctypes import c_int , c_double , c_float, c_ushort

dArray = npct.ndpointer(dtype=np.double,ndim=1,flags='CONTIGUOUS')  
sArray = npct.ndpointer(dtype=np.single,ndim=1,flags='CONTIGUOUS')  
iArray = npct.ndpointer(dtype=np.intc,ndim=1,flags='CONTIGUOUS')  
libnmf = npct.load_library("libnnmfpack","/home/pau/tmp/lib")




# ----------------------------------CPU NMF_General------------------------------------------
#Argument definitions
libnmf.dfhals_cpu.restype= c_int
libnmf.dfhals_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray,c_int]

libnmf.dmlsa_cpu.restype= c_int
libnmf.dmlsa_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray,c_int,c_int]
libnmf.smlsa_cpu.restype= c_int
libnmf.smlsa_cpu.argtypes= [c_int,c_int, c_int, sArray,sArray,sArray,c_int,c_int]

libnmf.dGCD_cpu.restype= c_int
libnmf.dGCD_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray,c_int,c_double]

libnmf.danlsbpp_cpu.restype= c_int
libnmf.danlsbpp_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray,c_int]

#Interface functions definition
def dfhals_cpu(m, n, k, A, W, H, nIter):
    return libnmf.dfhals_cpu(m, n, k, A, W, H, nIter)

def dbdiv_cpu(m, n,k, A, W, H, uType, nIter):
    return libnmf.dbdiv_cpu(m, n,k, A, W, H, uType, nIter)
def sbdiv_cpu(m, n,k, A, W, H, uType, nIter):
    return libnmf.sbdiv_cpu(m, n,k, A, W, H, uType, nIter)

def dGCD_cpu(m, n, k, A, W, H, nIter):
    return libnmf.dGCD_cpu(m, n, k, A, W, H, nIter)

def danlsbpp_cpu(m, n, k, A, W, H, nIter):
    return libnmf.danlsbpp_cpu(m, n, k, A, W, H, nIter)

# ----------------------------------CPU NMF_Specific------------------------------------------
#Argument definitions
libnmf.dbdiv_cpu.restype= c_int
libnmf.dbdiv_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray, c_double,c_int,c_int]
libnmf.sbdiv_cpu.restype= c_int
libnmf.sbdiv_cpu.argtypes= [c_int,c_int, c_int, sArray,sArray,sArray, c_float,c_int,c_int]

libnmf.dbdivRestrict_cpu.restype= c_int
libnmf.dbdivRestrict_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray, c_double,c_int,c_int,c_double, c_double, c_ushort]
libnmf.sbdivRestrict_cpu.restype= c_int
libnmf.sbdivRestrict_cpu.argtypes= [c_int,c_int, c_int, sArray,sArray,sArray, c_float,c_int,c_int,c_float, c_float, c_ushort]

libnmf.dAffine_cpu.restype= c_int
libnmf.dAffine_cpu.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray, dArray, c_int]

libnmf.derrorbd_x86.restype=c_double
libnmf.derrorbd_x86.argtypes= [c_int,c_int, c_int, dArray,dArray,dArray, c_double]
libnmf.serrorbd_x86.restype=c_float
libnmf.serrorbd_x86.argtypes= [c_int,c_int, c_int, sArray,sArray,sArray, c_float]

#Interface functions definition
def dbdiv_cpu(m, n,k, A, W, H, beta, uType, nIter):
    return libnmf.dbdiv_cpu(m, n,k, A, W, H, beta, uType, nIter)
def sbdiv_cpu(m, n,k, A, W, H, beta, uType, nIter):
    return libnmf.sbdiv_cpu(m, n,k, A, W, H, beta, uType, nIter)

def dbdivRestrict_cpu(m, n,k, A, W, H, beta, uType, nIter, alphaW, alphaH, restr):
    return libnmf.dbdivRestrict_cpu(m, n,k, A, W, H, beta, uType, nIter, alphaW, alphaH, restr)
def sbdivRestrict_cpu(m, n,k, A, W, H, beta, uType, nIter, alphaW, alphaH, restr):
    return libnmf.sbdivRestrict_cpu(m, n,k, A, W, H, beta, uType, nIter, alphaW, alphaH, restr)

def dAffine_cpu(m, n, k, A, W, H, w, nIter):
    return libnmf.dAffine_cpu(m, n, k, A, W, H, w, nIter)


def derrorbd_x86(m, n, k, A, W, H, beta):
    return libnmf.derrorbd_x86(m, n, k, A, W, H, beta)
def serrorbd_x86(m, n, k, A, W, H, beta):
    return libnmf.serrorbd_x86(m, n, k, A, W, H, beta)


# ----------------------------------CPU NMF_Specific------------------------------------------
#Argument definitions
libnmf.dbpp_cpu.restype= c_int
libnmf.dbpp_cpu.argtypes= [c_int,c_int, c_int, dArray, dArray, dArray]

libnmf.dasna_cpu.restype= c_int
libnmf.dasna_cpu.argtypes= [c_int,c_int, c_int, dArray, dArray, dArray,c_int, c_int]


def dbpp_cpu(q, r, p, C, B, X):
    return libnmf.dbpp_cpu(q, r, p, C, B, X)

def dasna_cpu(f, n ,o, Xini, Bini, W, nIter, nnz):
    return libnnmf.dasna_cpu(f, n ,o, Xini, Bini, W, nIter, nnz)

#------------------------------------- Utils_x86------------------------------------------
#Argument definitions
libnmf.dmemset_x86.restype = None
libnmf.dmemset_x86.argtypes = [c_int, dArray, c_double]
libnmf.smemset_x86.restype = None
libnmf.smemset_x86.argtypes = [c_int, sArray, c_float]
libnmf.imemset_x86.restype = None
libnmf.imemset_x86.argtypes = [c_int, iArray, c_int]

libnmf.dlarngenn_x86.restype = None
libnmf.dlarngenn_x86.argtypes = [c_int,c_int,c_int,dArray]
libnmf.slarngenn_x86.restype = None
libnmf.slarngenn_x86.argtypes = [c_int,c_int,c_int,sArray]

libnmf.derror_x86.restype = c_double
libnmf.derror_x86.argtypes = [c_int,c_int,c_int,dArray, dArray, dArray]
libnmf.serror_x86.restype = c_float
libnmf.serror_x86.argtypes = [c_int,c_int,c_int, sArray, sArray, sArray]

libnmf.dnrm2Cols_x86.restype = None
libnmf.dnrm2Cols_x86.argtypes = [c_int,c_int,dArray]

libnmf.dtrans_x86.restype = None
libnmf.dtrans_x86.argtypes = [c_int,c_int, dArray, dArray]

libnmf.dhalfwave_x86.restype = None
libnmf.dhalfwave_x86.argtypes = [c_int, dArray, c_int]

libnmf.dMin_x86.restype = c_int
libnmf.dMin_x86.argtypes = [c_int, dArray]

#Interface functions definition
def dlarngenn_x86(m,n,seed,M):
    return libnmf.dlarngenn_x86(m,n,seed,M)
def slarngenn_x86(m,n,seed,M):
    return libnmf.slarngenn_x86(m,n,seed,M)

def dmemset_x86(n, x, val):
    return libnmf.dmemset_x86(n, x, val)
def smemset_x86(n, x, val):
    return libnmf.smemset_x86(n, x, val)
def imemset_x86(n, x, val):
    return libnmf.imemset_x86(n, x, val)

def derror_x86(m, n, k, A, W, H):
    return libnmf.derror_x86(m, n, k, A, W, H)
def serror_x86(m, n, k, A, W, H):
    return libnmf.serror_x86(m, n, k, A, W, H)

def dnrm2Cols_x86(m, n, M):
    return libnmf.dnrm2Cols_x86(m, n, M)

def dtrans_x86(m, n, M, Mt):
    return libnmf.dtrans_x86(m, n, M, Mt)

def dhalfwave_x86(n, x, incx):
    return libnmf.dhalfwave_x86(n, x, incx)

def dMin_x86(n, x):
    return libnmf.dMin_x86(n, x)
