/**
 *  \file    anlsbpp_cpu.c
 *  \brief   File with functions to calcule NNMF using the ANLS-BPP algorithm for CPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/

#include "anlsbpp_cpu.h"

/** TODO Documentar
 * 
 * 
 * 
 *
 * */
int danlsbpp_cpu(const int m, const int n, const int k, const double* A, double *W, double *H, const int nIter)
{
   double *At=NULL, *Wt=NULL, *Ht=NULL;
   int     i;

   At = (double *)nmf_alloc(n * m * sizeof *At, WRDLEN);
   Wt = (double *)nmf_alloc(k * m * sizeof *Wt, WRDLEN);
   Ht = (double *)nmf_alloc(n * k * sizeof *Ht, WRDLEN);

   if (At == NULL || Wt == NULL || Ht == NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      //min_{H>=0} ||WH-A||_F^2
      dbpp_cpu(k, n, m, W, A, H);
        
      //min_{W>=0} ||H'W'-A'||_F^2
      //TODO transpose algorithm instead of matrices
      dtrans_cpu(m, n, A, At);
      dtrans_cpu(k, n, H, Ht);
      dbpp_cpu  (k, m, n, Ht, At, Wt);
      dtrans_cpu(k, m, Wt, W);
   }    

   nmf_free(At);
   nmf_free(Wt);
   nmf_free(Ht);

   return 0;
}

/** TODO Documentar
 * 
 * 
 * 
 *
 * */
int sanlsbpp_cpu(const int m, const int n, const int k, const float* A, float *W, float *H, const int nIter)
{
   float *At=NULL, *Wt=NULL, *Ht=NULL;
   int    i;

   At = (float *)nmf_alloc(n * m * sizeof *At, WRDLEN);
   Wt = (float *)nmf_alloc(k * m * sizeof *Wt, WRDLEN);
   Ht = (float *)nmf_alloc(n * k * sizeof *Ht, WRDLEN);

   if (At == NULL || Wt == NULL || Ht == NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      //min_{H>=0} ||WH-A||_F^2
      sbpp_cpu(k, n, m, W, A, H);
        
      //min_{W>=0} ||H'W'-A'||_F^2
      //TODO transpose algorithm instead of matrices
      strans_cpu(m, n, A, At);
      strans_cpu(k, n, H, Ht);
      sbpp_cpu  (k, m, n, Ht, At, Wt);
      strans_cpu(k, m, Wt, W);
   }    

   nmf_free(At);
   nmf_free(Wt);
   nmf_free(Ht);

   return 0;
}
