/**
 *  \file    anlsbpp_cpu.h
 *  \brief   Header file for using the ANLS-BPP algorithm  with CPU
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef ANLSBPP_CPU_H
#define ANLSBPP_CPU_H

#include <utils_cpu.h>
#include <bpp_cpu.h>
#include "general_cpu.h"//Public interface of the library

#endif
