/**
 *  \file    fhals_cpu.h
 *  \brief   Header file for using the fHALS algorithm  with CPU
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef HALS_CPU_H
#define HALS_CPU_H

#include <utils_cpu.h>
#include "general_cpu.h"//Public interface of the library

#endif
