/**
 *  \file    general_cpu.h
 *  \brief   Interface file for the General NMF decomposition algorithms
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef GENERAL_CPU_H
#define GENERAL_CPU_H

int dfhals_cpu  (const int, const int, const int, const double *, double *, double *, const int ); 
int sfhals_cpu  (const int, const int, const int, const float  *, float  *, float  *, const int ); 
int dmlsa_cpu   (const int, const int, const int, const double *, double *, double *, const int );
int smlsa_cpu   (const int, const int, const int, const float  *, float  *, float  *, const int );
int dGCD_cpu    (const int, const int, const int, const double *, double *, double *, const int, const double); 
int sGCD_cpu    (const int, const int, const int, const float  *, float  *, float  *, const int, const float); 
int danlsbpp_cpu(const int, const int, const int, const double *, double *, double *, const int );
int sanlsbpp_cpu(const int, const int, const int, const float  *, float  *, float  *, const int );

#endif
