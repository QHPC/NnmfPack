/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    bdiv_cpu.c
 *  \brief   File with functions to calcule NNMF using betadivergence methods for CPUs
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com  
 *  \date    04/11/14
*/

#include "bdiv_cpu.h"

/**
 *  \fn    int dbdivg_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double expo, const int uType, const int nIter)
 *  \brief dbdivg_cpu performs the NNMF using beta-divergence when beta is != 1 and !=2, using double precision.
 *
 *         The algorithm is<BR>
 *         &nbsp;&nbsp;repit nIter times<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L(.^)(beta-2)<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L2=L1(.*)A<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L1(.*)L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B=W'*L2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C=W'*L1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H=H(.*)B(./)C<BR>
 *
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L(.^)(beta-2)<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L2=L1(.*)A<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L1(.*)L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D=L2*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E=L1*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W=W(.*)D(./)E<BR>
 *         &nbsp;&nbsp;end repit<BR>
 *         End algorithm<BR>
 *
 *         In real live L1 and L2 are (m*n) matrices used in STEP 1 and 2. For performance
 *         reasons only one 1D colum-mayor buffer, named R in next code, of size 2*m*n is used
 *         In STEP 1, first part of R (m*n positions) is L2 and the second part is L1.
 *         In STEP 2, fisrt column of R (2*m positions) is composed by the first column of L2
 *         following first column of L1. Second column of R (2*m positions) is composed by the
 *         sencond column of L2 following second column of L1. 3rd column of R ... and so on
 *
 *         In real live B and C are (k*n) matrices used in STEP 1, and D and E are (m*k)
 *         matrices used in STEP 2. B/C and D/E are independent. However we do not have L1
 *         and L2, we have R, and we can do B=W'*L2 and C=W'*L1 (or D=L2*H' and E=L1*H') at
 *         the same time. For this reason only one matrix is declared to save space. This is
 *         matrix M with size 2*max(m,n)*k
 *
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param expo:  (input) Double precision value. The exponent beta of betadivergence method
 *  \param nIter: (input) Number of iterations
 *
 *  It returns 0 if all is OK.
*/
int dbdivg_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double expo, int nIter)
{
   double *L=NULL, *M=NULL, *R=NULL;
   int     i;

   M = (double *)nmf_alloc(2*max(m,n)* k * sizeof *M, WRDLEN);
   L = (double *)nmf_alloc(m         * n * sizeof *L, WRDLEN);
   R = (double *)nmf_alloc(2*m       * n * sizeof *R, WRDLEN);

   if (L==NULL || M==NULL || R==NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      dkernelH_cpu(m, n, L, A, R, expo-2.0);

      /* B=W'*L2 */
      /* C=W'*L1 */
      /* above is equal to R=W'*|L2 | L1| */
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

      /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
      dupdate1H_cpu(k*n, M, H);


      /* ************************ Phase 2 *************************** */
      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      dkernelW_cpu(m, n, L, A, R, expo-2.0);   

      /* D=L2*H' */
      /* E=L1*H' */
      /*                     |L2|      */
      /* above is equal to R=|  | * H' */
      /*                     |L1|      */   
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);

      /* W=W(.*)D(./)E */
      dupdate1W_cpu(m, k, M, W);
   }

   nmf_free(L);
   nmf_free(R);
   nmf_free(M);

   return 0;
}


/**
 *  \fn    int sbdivg_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float expo, const int uType, const int nIter)
 *  \brief sbdivg_cpu performs NNMF using betadivergence for general case (beta <> 1 and 2) using simple precision
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param expo:  (input) Simple precision value. The exponent beta of betadivergence method
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise 
*/
int sbdivg_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float expo, int nIter)
{
   float *L=NULL, *M=NULL, *R=NULL;
   int    i;

   M = (float *)nmf_alloc(2*max(m,n)* k * sizeof *M, WRDLEN);
   L = (float *)nmf_alloc(m         * n * sizeof *L, WRDLEN);
   R = (float *)nmf_alloc(2*m       * n * sizeof *R, WRDLEN);

   if (L == NULL || M == NULL || R == NULL) {  return -1; }

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      skernelH_cpu(m, n, L, A, R, expo-2.0f);

      /* B=W'*L2 */
      /* C=W'*L1 */
      /* above is equal to R=W'*|L2 | L1| */
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

      /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
      supdate1H_cpu(k*n, M, H);


      /* ************************ Phase 2 *************************** */
      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      skernelW_cpu(m, n, L, A, R, expo-2.0f);

      /* D=L2*H' */
      /* E=L1*H' */
      /*                     |L2|      */
      /* above is equal to R=|  | * H' */
      /*                     |L1|      */   
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);

      /* W=W(.*)D(./)E */
      supdate1W_cpu(m, k, M, W);
   }

   nmf_free(L);
   nmf_free(R);
   nmf_free(M);

   return 0;
}


/** TODO documentar
 * 
 */
int dbdivgRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double expo,
                       const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   double *L=NULL, *M=NULL, *R=NULL;
   int     i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0) { return dbdivg_cpu(m, n, k, A, W, H, expo, nIter); }

   if(restr > 4) { return -2; }

   M = (double *)nmf_alloc(2*max(m,n)* k * sizeof *M, WRDLEN);
   L = (double *)nmf_alloc(m         * n * sizeof *L, WRDLEN);
   R = (double *)nmf_alloc(2*m       * n * sizeof *R, WRDLEN);

   if (L == NULL || M == NULL || R == NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      dkernelH_cpu(m, n, L, A, R, expo-2.0);

      /* B=W'*L2 */
      /* C=W'*L1 */
      /* above is equal to R=W'*|L2 | L1| */
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

      switch (restr)
      {
         case 1:
         case 4:
            drestr1norm_cpu(k*n, alphaH, M);
            break;
         case 2:
         case 3:
            drestr2norm_cpu(k*n, alphaH, H, M);
            break;
      }

      /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
      dupdate1H_cpu(k*n, M, H);


      /* ************************ Phase 2 *************************** */
      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      dkernelW_cpu(m, n , L, A, R, expo-2.0);   

      /* D=L2*H' */
      /* E=L1*H' */
      /*                     |L2|      */
      /* above is equal to R=|  | * H' */
      /*                     |L1|      */   
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);
           
      switch (restr)
      {
         case 1:
         case 3:
            drestr1normSub_cpu(m, k, alphaW, M, 2*m);
            break;
         case 2:
         case 4:
            drestr2normSub_cpu(m, k, alphaW, W, m, M, 2*m);
            break;
      }
            
      /* W=W(.*)D(./)E */
      dupdate1W_cpu(m, k, M, W);
   }

   nmf_free(L);
   nmf_free(R);
   nmf_free(M);

   return 0;
}


/** TODO documentar 
 * 
 */ 
int sbdivgRestrict_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float expo,
                       const int nIter, const float alphaW, const float alphaH, const unsigned short restr)
{
   float *L=NULL, *M=NULL, *R=NULL;
   int    i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0) { return sbdivg_cpu(m, n, k, A, W, H, expo, nIter); }

   if(restr > 4) { return -2; }

   M = (float *)nmf_alloc(2*max(m,n)* k * sizeof *M, WRDLEN);
   L = (float *)nmf_alloc(m         * n * sizeof *L, WRDLEN);
   R = (float *)nmf_alloc(2*m       * n * sizeof *R, WRDLEN);

   if (L == NULL || M == NULL || R == NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      skernelH_cpu(m, n, L, A, R, expo-2.0f);

      /* B=W'*L2 */
      /* C=W'*L1 */
      /* above is equal to R=W'*|L2 | L1| */
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

      switch (restr)
      {
         case 1:
         case 4:
            srestr1norm_cpu(k*n, alphaH, M);
            break;
         case 2:
         case 3:
            srestr2norm_cpu(k*n, alphaH, H, M);
            break;
      }

      /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
      supdate1H_cpu(k*n, M, H);


      /* ************************ Phase 2 *************************** */
      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

      /* L1=L(.^)(expo-2) */
      /* L2=L1(.*)A       */
      /* L1=L1(.*)L       */
      /* R is L1 and L2   */
      skernelW_cpu(m, n , L, A, R, expo-2.0f);   

      /* D=L2*H' */
      /* E=L1*H' */
      /*                     |L2|      */
      /* above is equal to R=|  | * H' */
      /*                     |L1|      */   
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);
           
      switch (restr)
      {
         case 1:
         case 3:
            srestr1normSub_cpu(m, k, alphaW, M, 2*m);
            break;
         case 2:
         case 4:
            srestr2normSub_cpu(m, k, alphaW, W, m, M, 2*m);
            break;
      }
            
      /* W=W(.*)D(./)E */
      supdate1W_cpu(m, k, M, W);
   }

   nmf_free(L);
   nmf_free(R);
   nmf_free(M);

   return 0;
}


/** TODO añadir comentario
 * restr. 0: nada, 1: WH 1norma, 2: WH 2norma, 3: W 1norma H 2norma, 4: W 2norma H 1norma 
 */
int dmlsaRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int nIter,
                      const double alphaW, const double alphaH, const unsigned short restr)
{
   double *N=NULL, *D=NULL, *Aux=NULL;
   int    i;
  
   if ((alphaW == 0 && alphaH == 0) || restr == 0) { return dmlsa_cpu(m, n, k, A, W, H, nIter); }

   if(restr > 4) { return -2; }
    
   N =   (double *)nmf_alloc(max(m,n) * k * sizeof *N,   WRDLEN);
   D =   (double *)nmf_alloc(max(m,n) * k * sizeof *D,   WRDLEN);
   Aux = (double *)nmf_alloc(k        * k * sizeof *Aux, WRDLEN);
	
   if (N == NULL || D == NULL || Aux == NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* N=W'*A */	
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, N, k);		
            
      switch (restr)
      {
         case 1:
         case 4:
            drestr1norm_cpu(k*n, alphaH, N);
            break;
         case 2:
         case 3:
            drestr2norm_cpu(k*n, alphaH, H, N);
            break;
      }

      /* Aux=W'*W */
      cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Aux, k);

      /* D=Aux*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, Aux, k, H, k, 0.0, D, k);

      /* H=H(*)N(/)D */
      ddotdiv_cpu(k*n, N, D, H);


      /* ************************ Phase 2 *************************** */
      /* N=A*H' */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, A, m, H, k, 0.0, N, m);

      switch (restr)
      {
         case 1:
         case 3:
            drestr1norm_cpu(m*k, alphaW, N);
            break;
         case 2:
         case 4:
            drestr2norm_cpu(m*k, alphaW, W, N);
            break;
      }

      /* Aux=H*H' */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, Aux, k);

      /* D=W*Aux */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, Aux, k, 0.0, D, m);

      /* W=W(*)N(/)D */
      ddotdiv_cpu(m*k, N, D, W);

   }
   nmf_free(N);
   nmf_free(D);
   nmf_free(Aux);

   return 0;
}


/** TODO añadir comentario
 * restr. 0: nada, 1: WH 1norma, 2: WH 2norma, 3: W 1norma H 2norma, 4: W 2norma H 1norma 
 */
int smlsaRestrict_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int nIter,
                      const float alphaW, const float alphaH, const unsigned short restr )
{
   float *N=NULL, *D=NULL, *Aux=NULL;
   int    i;
  
   if ((alphaW == 0 && alphaH == 0) || restr == 0) { return smlsa_cpu(m, n, k, A, W, H, nIter); }

   if(restr > 4) { return -2; }

   N =   (float *)nmf_alloc(max(m,n) * k * sizeof *N,   WRDLEN);
   D =   (float *)nmf_alloc(max(m,n) * k * sizeof *D,   WRDLEN);
   Aux = (float *)nmf_alloc(k        * k * sizeof *Aux, WRDLEN);
    
   if (N == NULL || D == NULL || Aux == NULL) { return -1; }

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* N=W'*A */	
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, N, k);		
            
      switch (restr)
      {
         case 1:
         case 4:
            srestr1norm_cpu(k*n, alphaH, N);
            break;
         case 2:
         case 3:
            srestr2norm_cpu(k*n, alphaH, H, N);
            break;
      }

      /* Aux=W'*W */
      cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Aux, k);

      /* D=Aux*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, Aux, k, H, k, 0.0, D, k);

      /* H=H(*)N(/)D */
      sdotdiv_cpu(k*n, N, D, H);


      /* ************************ Phase 2 *************************** */
      /* N=A*H' */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, A, m, H, k, 0.0, N, m);

      switch (restr)
      {
         case 1:
         case 3:
            srestr1norm_cpu(m*k, alphaW, N);
            break;
         case 2:
         case 4:
            srestr2norm_cpu(m*k, alphaW, W, N);
            break;
      }

      /* Aux=H*H' */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, Aux, k);

      /* D=W*Aux */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, Aux, k, 0.0, D, m);

      /* W=W(*)N(/)D */
      sdotdiv_cpu(m*k, N, D, W);
   }
   nmf_free(N);
   nmf_free(D);
   nmf_free(Aux);

   return 0;
}


/**
 *  \fn    int dbdivone_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter)
 *  \brief dbdivone_cpu performs NNMF using beta-divergence when beta=1, using double precision
 *
 *         The algorithm is<BR>
 *         &nbsp;&nbsp;repit nIter times<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;y(i)=sum(W(j,i) for all j in range) for all i in range<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=A(./)L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B=W'*L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B(i,j)=B(i,j) / y(i) for all B elements<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H=H(.*)B<BR>
 *
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;y(i)=sum(H(i,j) for all j in range) for all i in range<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=A(./)L <BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D=L*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B(i,j)=B(i,j) / y(j) for all B elements<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W=W(.*)D<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;end repit<BR>
 *         End algorithm<BR>
 *
 *         In real live B is a (k*n) matrix used in STEP 1, and D is a (m*k)
 *         matrix used in STEP 2. B and D are independent. For this reason only 1 matrix
 *         of size max(m,n)*k is declared/used
 *
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param nIter: (input) Number of iterations
 *
 *  It returns 0 if all is OK.
*/
int dbdivone_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int nIter)
{
   double *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int     i;

   /* Vectors "x" and "y" are used both in Phase 1 and Phase 2. With   */
   /* the strategy used for matrices B and D the sise of x is max(m,n) */
   B = (double *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
   L = (double *)nmf_alloc(m * n        * sizeof *L, WRDLEN);
   x = (double *)nmf_alloc(max(m,n)     * sizeof *x, WRDLEN);
   y = (double *)nmf_alloc(k            * sizeof *y, WRDLEN);

   if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

   /* x[i]=1.0 for all i */
   dmemset_cpu(max(m,n), x, 1.0);

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* Calculate the sums of all W columns via dgemv(W, x) */
      cblas_dgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      ddiv_cpu(m*n, A, L, L);

      /* B=W'*L */
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

      /* B(i,j)=B(i,j)/y(i) for all B elements */
      /* H=H(.*)B */
      dupdate2H_cpu(k, n, y, B, H);


      /* ************************ Phase 2 *************************** */
      /* Calculate the sums of all H rows via dgemv(H, x) */
      cblas_dgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

       /* L=W*H */
       cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

       /* L=A(./)L*/
       ddiv_cpu(m*n, A, L, L);

       /* B=L*H' */
       cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

       /* B(i,j)=B(i,j)/y(j) for all B elements */
       /* W=W(.*)B */
       dupdate2W_cpu(m, k, y, B, W);
   }
   nmf_free(B);
   nmf_free(L);
   nmf_free(x);
   nmf_free(y);

   return 0;
}


/**
 *  \fn    int sbdivone_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType, const int nIter)
 *  \brief sbdivone_cpu performs NNMF using betadivergence when beta=1 using simple precision
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise  
*/
int sbdivone_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int nIter)
{
   float *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int   i;

   B = (float *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
   L = (float *)nmf_alloc(m * n        * sizeof *L, WRDLEN);
   x = (float *)nmf_alloc(max(m,n)     * sizeof *x, WRDLEN);
   y = (float *)nmf_alloc(k            * sizeof *y, WRDLEN);

   if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

   /* x[i]=1.0 for all i */
   smemset_cpu(max(m,n), x, 1.0);

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */    
      /* Calculate the sums of all W columns via sgemv(W, x) */
      cblas_sgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      sdiv_cpu(m*n, A, L, L);

      /* B=W'*L */
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

      /* B(i,j)=B(i,j)/y(i) for all B elements */
      /* H=H(.*)B */
      supdate2H_cpu(k, n, y, B, H);

	
      /* ************************ Phase 2 *************************** */
      /* Calculate the sums of all H rows via sgemv(H, x) */
      cblas_sgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      sdiv_cpu(m*n, A, L, L);

      /* B=L*H' */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

      /* B(i,j)=B(i,j)/y(j) for all B elements */
      /* W=W(.*)B */
      supdate2W_cpu(m, k, y, B, W);
   }
   nmf_free(B);
   nmf_free(L);
   nmf_free(x);
   nmf_free(y);

   return 0;
}


/**TODO documentar
 * 
 */
int dbdivoneRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, 
                         const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   double *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int     i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0) { return dbdivone_cpu(m, n, k, A, W, H, nIter); }

   if(restr > 4) { return -2; }


   /* Vectors "x" and "y" are used both in Phase 1 and Phase 2. With   */
   /* the strategy used for matrices B and D the sise of x is max(m,n) */
   B = (double *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
   L = (double *)nmf_alloc(m * n        * sizeof *L, WRDLEN);
   x = (double *)nmf_alloc(max(m,n)     * sizeof *x, WRDLEN);
   y = (double *)nmf_alloc(k            * sizeof *y, WRDLEN);

   if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

   /* x[i]=1.0 for all i */
   dmemset_cpu(max(m,n), x, 1.0);

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* Calculate the sums of all W columns via dgemv(W, x) */
      cblas_dgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      ddiv_cpu(m*n, A, L, L);

      /* B=W'*L */
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

      switch (restr)
      {
         case 1:
         case 4:
            drestr1norm_cpu(k*n, alphaH, B);
            break;
         case 2:
         case 3:
            drestr2norm_cpu(k*n, alphaH, H, B);
            break;
      }

      /* B(i,j)=B(i,j)/y(i) for all B elements */
      /* H=H(.*)B */
      dupdate2H_cpu(k, n, y, B, H);


      /* ************************ Phase 2 *************************** */
      /* Calculate the sums of all H rows via dgemv(H, x) */
      cblas_dgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      ddiv_cpu(m*n, A, L, L);

      /* B=L*H' */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

      switch (restr)
      {
         case 1:
         case 3:
            drestr1norm_cpu(m*k, alphaW, B);
            break;
         case 2:
         case 4:
            drestr2norm_cpu(m*k, alphaW, W, B);
            break;
      }

      /* B(i,j)=B(i,j)/y(j) for all B elements */
      /* W=W(.*)B */
      dupdate2W_cpu(m, k, y, B, W);
   }
   nmf_free(B);
   nmf_free(L);
   nmf_free(x);
   nmf_free(y);

   return 0;
}


/** TODO documentar 
 * 
 */
int sbdivoneRestrict_cpu(const int m, const int n, const int k, const float  *A, float  *W, float  *H,
                         const int nIter, const float alphaW, const float alphaH, const unsigned short restr)
{
   float *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0) { return sbdivone_cpu(m, n, k, A, W, H, nIter); }

   if(restr > 4) { return -2; }


   /* Vectors "x" and "y" are used both in Phase 1 and Phase 2. With   */
   /* the strategy used for matrices B and D the sise of x is max(m,n) */
   B = (float *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
   L = (float *)nmf_alloc(m * n        * sizeof *L, WRDLEN);
   x = (float *)nmf_alloc(max(m,n)     * sizeof *x, WRDLEN);
   y = (float *)nmf_alloc(k            * sizeof *y, WRDLEN);

   if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

   /* x[i]=1.0 for all i */
   smemset_cpu(max(m,n), x, 1.0);

   for(i=0; i<nIter; i++)
   {
      /* ************************ Phase 1 *************************** */
      /* Calculate the sums of all W columns via dgemv(W, x) */
      cblas_sgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      sdiv_cpu(m*n, A, L, L);

      /* B=W'*L */
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

      switch (restr)
      {
         case 1:
         case 4:
            srestr1norm_cpu(k*n, alphaH, B);
            break;
         case 2:
         case 3:
            srestr2norm_cpu(k*n, alphaH, H, B);
            break;
      }

      /* B(i,j)=B(i,j)/y(i) for all B elements */
      /* H=H(.*)B */
      supdate2H_cpu(k, n, y, B, H);


      /* ************************ Phase 2 *************************** */
      /* Calculate the sums of all H rows via dgemv(H, x) */
      cblas_sgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

      /* L=W*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

      /* L=A(./)L*/
      sdiv_cpu(m*n, A, L, L);

      /* B=L*H' */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

      switch (restr)
      {
         case 1:
         case 3:
            srestr1norm_cpu(m*k, alphaW, B);
            break;
         case 2:
         case 4:
            srestr2norm_cpu(m*k, alphaW, W, B);
            break;
      }

      /* B(i,j)=B(i,j)/y(j) for all B elements */
      /* W=W(.*)B */
      supdate2W_cpu(m, k, y, B, W);
   }
   nmf_free(B);
   nmf_free(L);
   nmf_free(x);
   nmf_free(y);

   return 0;
}


/**
 *  \fn    int dbdiv_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double beta, const int uType, const int nIter)
 *  \brief dbdiv_cpu is a wrapper that calls the adequate function to performs NNMF using betadivergence using double precision with CPU
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param beta:  (input) Double precision value. The parameter beta of betadivergence method
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise  
*/
int dbdiv_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double beta, const int nIter)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if (beta>=2.0 && beta<=2.0)
     return dmlsa_cpu(m, n, k, A, W, H, nIter);
   else
   {
     if (beta>=1.0 && beta<=1.0)
       return dbdivone_cpu(m, n, k, A, W, H, nIter);
     else
       return dbdivg_cpu(m, n, k, A, W, H, beta, nIter);
   }
}


/**
 *  \fn    int sbdiv_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const float beta, const int uType, const int nIter)
 *  \brief sbdiv_cpu is a wrapper that calls the adequate function to performs NNMF using betadivergence using simple precision with CPU
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param beta:  (input) Simple precision value. The parameter beta of betadivergence method
 *  \param nIter: (input) Number of iterations
*/
int sbdiv_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float beta, const int nIter)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if (beta>=2.0 && beta<=2.0)
     return smlsa_cpu(m, n, k, A, W, H, nIter);
   else
   {
     if (beta>=1.0 && beta<=1.0)
       return sbdivone_cpu(m, n, k, A, W, H, nIter);
     else
       return sbdivg_cpu(m, n, k, A, W, H, beta, nIter);
   }
}


/** TODO documentar
 * 
 */
int dbdivRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double beta, 
                      const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if (beta>=2.0 && beta<=2.0)
     return dmlsaRestrict_cpu(m, n, k, A, W, H, nIter, alphaW, alphaH, restr);
   else
   {
     if (beta>=1.0 && beta<=1.0)
       return dbdivoneRestrict_cpu(m, n, k, A, W, H, nIter, alphaW, alphaH, restr);
     else
       return dbdivgRestrict_cpu(m, n, k, A, W, H, beta, nIter, alphaW, alphaH, restr);
   }
}


/** TODO documentar
 * 
 */
int sbdivRestrict_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float beta, 
                      const int nIter, const float alphaW, const float alphaH, const unsigned short restr)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if (beta>=2.0 && beta<=2.0)
     return smlsaRestrict_cpu(m, n, k, A, W, H, nIter, alphaW, alphaH, restr);
   else
   {
     if (beta>=1.0 && beta<=1.0)
       return sbdivoneRestrict_cpu(m, n, k, A, W, H, nIter, alphaW, alphaH, restr);
     else
       return sbdivgRestrict_cpu(m, n, k, A, W, H, beta, nIter, alphaW, alphaH, restr);
   }
}


/**
 *  \fn    void dkernelH_cpu(const int m, const int n, const double *L, const double *A, double *__restrict__ R, const int m, const int n, const double expo)
 *  \brief This function performs double precision R(i)=(L(i)^expo)*A[i] and R(i+m*n)=L[i]*(L(i)^expo)
 *         Note expo is a real number expo < 0 or expo > 0  
 *  \param m:    (input)  Number of rows    of L, A and R matrices
 *  \param n:    (input)  Number of columns of L, A and R matrices
 *  \param L:    (input)  Double precision input matrix (1D column-major)
 *  \param A:    (input)  Double precision input matrix (1D column-major)
 *  \param R:    (output) Double precision output matrix (1D column-major)
 *  \param expo: (input)  the "power of" for function pow(). It is a double precision value
*/
void dkernelH_cpu(const int m, const int n, const double *L, const double *A, double *__restrict__ R, const double expo)
{
   int i, size=m*n;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif	
   for(i=0; i<size; i++)
   {
     double dtmp1, dtmp2;

     if (L[i]>=0.0 && L[i]<=0.0)
       R[i] = R[i+size] = 0.0;
     else
     {
       dtmp1=L[i];
       dtmp2=pow(dtmp1, expo);

       /* ask for A[i]=0.0 don't give improvements. We don't do it */   
       R[i]     =dtmp2 * A[i];
       R[i+size]=dtmp1 * dtmp2; 
     }
   }
}


/**
 *  \fn    void skernelH_cpu(const int m, const int n, const float *L, const float *A, float *__restrict__ R, const float expo)
 *  \brief This function computes simple precision R(i)=(L(i)^expo)*A[i] and R(i+m*n)=L[i]*(L(i)^expo)
 *         Note "expo" is a real number expo < 0 or expo > 0
 *  \param m:    (input)  Number of rows    of L, A and R matrices
 *  \param n:    (input)  Number of columns of L, A and R matrices
 *  \param L:    (input)  Simpel precision input matrix (1D column-major)
 *  \param A:    (input)  Simpel precision input matrix (1D column-major)
 *  \param R:    (output) Simpel precision output matrix (1D column-major)
 *  \param expo: (input)  the "power of" for function pow(). It is a simple precision value
*/
void skernelH_cpu(const int m, const int n, const float *L, const float *A, float *__restrict__ R, const float expo)
{
   int i, size=m*n;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<size; i++)
   {
     float ftmp1, ftmp2;

     if (L[i]>=0.0 && L[i]<=0.0)
       R[i] = R[i+size] = 0.0;
     else
     {
       ftmp1=L[i];
       ftmp2=powf(ftmp1, expo);

       /* ask for A[i]=0.0 don't give improvements. We don't do it */   
       R[i]     =ftmp2 * A[i];
       R[i+size]=ftmp1 * ftmp2;
     }
   }
}


/**
 *  \fn    void dkernelW_cpu(const int m, const int n, const double *L, const double *A, double *__restrict__ R, const double expo)
 *  \brief This function computes double precision R(pos)=L(i)^expo)*A(i) and R(pos+m)=L(i)*(L(i)^expo)
 *         Note expo is a real number expo < 0 or expo > 0
 *  \param m:    (input)  Number of rows    of L, A and R matrices
 *  \param n:    (input)  Number of columns of L, A and R matrices
 *  \param L:    (input)  Double precision input matrix (1D column-major)
 *  \param A:    (input)  Double precision input  matrix (1D column-major)
 *  \param R:    (output) Double precision output matrix (1D column-major)
 *  \param expo: (input)  the "power of" for function pow(). It is a double precision value
*/
void dkernelW_cpu(const int m, const int n, const double *L, const double *A, double *__restrict__ R, const double expo)
{
   int i;

   #ifdef With_ICC
     #pragma parallel
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif	
   for(i=0; i<m*n; i++)
   {
     int pos;
     double dtmp1, dtmp2;

     pos=2*m*(i/m)+(i%m);

     if (L[i]>=0.0 && L[i]<=0.0)
       R[pos] = R[pos+m] = 0.0;
     else
     {
       dtmp1=L[i];
       dtmp2=pow(dtmp1, expo);

       /* ask for A[i]=0.0 don't give improvements. We don't do it */
       R[pos]  =dtmp2 * A[i];
       R[pos+m]=dtmp1 * dtmp2;
     }
   }
}


/**
 *  \fn    void skernelW_cpu(const int m, const int n, const float *L, const float *A, float *__restrict__ R, const float expo)
 *  \brief This function computes simple precision R(pos)=L(i)^expo)*A(i) and R(pos+m)=L(i)*(L(i)^expo)
 *         Note expo is a real number expo < 0 or expo > 0
 *  \param m:    (input)  Number of rows of A
 *  \param n:    (input)  Number of columns of A
 *  \param L:    (input)  Simple precision input matrix (1D column-major)
 *  \param A:    (input)  Simple precision input matrix (1D column-major)
 *  \param R:    (output) Simple precision output matrix (1D column-major)
 *  \param expo: (input)  the "power of" for function pow(). It is a simple precision value
*/
void skernelW_cpu(const int m, const int n, const float *L, const float *A, float *__restrict__ R, const float expo)
{
   int i;

   #ifdef With_ICC
     #pragma parallel
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif	
   for(i=0; i<m*n; i++)
   {
     int pos;
     float ftmp1, ftmp2;

     pos=2*m*(i/m)+(i%m);

     if (L[i]>=0.0 && L[i]<=0.0)
       R[pos] = R[pos+m] = 0.0;
     else
     {
       ftmp1=L[i];
       ftmp2=powf(ftmp1, expo);

       /* ask for A[i]=0.0 don't give improvements. We don't do it */
       R[pos]  =ftmp2 * A[i];
       R[pos+m]=ftmp1 * ftmp2;
     }
   }
}


/**
 *  \fn    dupdate1H_cpu(const int n, const double *X, double *__restrict__ H)
 *  \brief This function computes double precision H(i)=H(i)*B(i)/C(i) where matrices
 *         B and C are stored in the same buffer (called X). All B 1st and after C
 *  \param n: (input) Number of elements of H
 *  \param X: (input) Simple precision input matrix (1D column-major)
 *  \param H: (inout) Simple precision input/output matrix (1D column-major)
*/
void dupdate1H_cpu(const int n, const double *X, double *__restrict__ H)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if X(pos+n) and/or H(pos) and or/ X(pos)=0 */
       H[i]=H[i] * (X[i] / X[i+n]);
       assert(isfinite(H[i]));
     #else
       H[i]=H[i] * (X[i] / X[i+n]);
     #endif
   }
}

/**
 *  \fn    void supdate1H_cpu(const int n, const float *X, float *__restrict__ H)
 *  \brief This function computes simple precision H(i)=H(i)*B(i)/C(i) where matrices
 *         B and C are stored in the same buffer (called X). All B 1st and after C
 *  \param n: (input) Number of elements of H
 *  \param X: (input) Simple precision input matrix (1D column-major)
 *  \param H: (inout) Simple precision input/output matrix (1D column-major)
*/
void supdate1H_cpu(const int n, const float *X, float *__restrict__ H)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if X(i+n) and/or H(i) and/or X(i)=0 */
       H[i]=H[i] * (X[i] / X[i+n]);
       assert(isfinite(H[i]));
     #else
       H[i]=H[i] * (X[i] / X[i+n]);
     #endif
   }
}


/**
 *  \fn    void dupdate1W_cpu(const int m, const int n, const *double X, double *__restrict__ W)
 *  \brief This function computes double precision W[i]=W[i]*D[i]/E[i] where matrices D and E are 
 *         stored in the same buffer (called X) according beta-divergence general case (see dbdivg_cpu(...))
 *  \param m: (input) Number of rows    of W
 *  \param n: (input) Number of columns of W
 *  \param X: (input) Double precision input matrix (1D column-major)
 *  \param W: (inout) Double precision input/output matrix (1D column-major)
*/
void dupdate1W_cpu(const int m, const int n, const double *X, double *__restrict__ W)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<m*n; i++)
   {
     int pos;

     pos = i + (i/m)*m;

     #ifdef With_Check
       /* Here we can have NaN and Inf if X(pos+m) and/or W(i) and/or X(pos)=0 */
       W[i]=W[i] * (X[pos] / X[pos+m]);
       assert(isfinite(W[i]));
     #else
       W[i]=W[i] * (X[pos] / X[pos+m]);
     #endif
   }
}


/**
 *  \fn   void supdate1W_cpu(const int m, const int n, const float *X, float *__restrict__ W)
 *  \brief This function computes double precision W[i]=W[i]*D[i]/E[i] where matrices D and E are 
 *         stored in the same buffer (called X) according beta-divergence general case (see dbdivg_cpu(...))
 *  \param m: (input) Number of rows    of W
 *  \param n: (input) Number of columns of W
 *  \param X: (input) Simple precision input matrix (1D column-major)
 *  \param W: (inout) Simple precision input/output matrix (1D column-major)
*/
void supdate1W_cpu(const int m, const int n, const float *X, float *__restrict__ W)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
        #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<m*n; i++)
   {
     int pos;

     pos  = i + (i/m)*m;

     #ifdef With_Check
       /* Here we can have NaN and Inf if X(pos+m) and/or W(i) and/or X(pos)=0 */
       W[i]=W[i] * (X[pos] / X[pos+m]);
       assert(isfinite(W[i]));
     #else
       W[i]=W[i] * (X[pos] / X[pos+m]);
     #endif
   }
}


/**
 *  \fn    void dupdate2H_cpu(const int m, const int n, const double *y, const double *B, double *__restrict__ H)
 *  \brief This function computes double precision H(i)=H(i)*(B(i)/y(j))
 *  \param m:   (input) Number of rows    of B and H, and number of elements of vector y
 *  \param n:   (input) Number of columns of B and A
 *  \param y:   (input) Double precision vector with the sum of W columns
 *  \param B:   (input) Double precision input matrix (1D column-major)
 *  \param H:   (inout) Double precision input/output matrix (1D column-major)
*/
void dupdate2H_cpu(const int m, const int n, const double *y, const double *B, double *__restrict__ H)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<m*n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i%m) and/or H(i) and/or B(i)=0 */
       H[i]=H[i] * (B[i] / y[i%m]);
       assert(isfinite(H[i]));
     #else
       H[i]=H[i] * (B[i] / y[i%m]);
     #endif
   }
}


/**
 *  \fn    void supdate2H_cpu(const int m, const int n, const float *y, const float *B, float *__restrict__ H)
 *  \brief This function performs the simple H(i)=H(i)*(B(i)/y(j))
 *  \param m:   (input) Number of rows    of B and H, and number of elements of vector y
 *  \param n:   (input) Number of columns of B and A
 *  \param y:   (input) Simple precision vector with the sum of W columns
 *  \param B:   (input) Simple precision input matrix (1D column-major)
 *  \param H:   (inout) Simple precision input/output matrix (1D column-major)
*/
void supdate2H_cpu(const int m, const int n, const float *y, const float *B, float *__restrict__ H)
{
   int i;
  
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<m*n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i%m) and/or H(i) and/or B(i)=0 */
       H[i]=H[i] * (B[i] / y[i%m]);
       assert(isfinite(H[i]));
     #else
       H[i]=H[i] * (B[i] / y[i%m]);
     #endif
   }
}


/**
 *  \fn    void dupdate2W_cpu(const int m, const int n, const double *y, const double *B, double *__restrict__ W)
 *  \brief This function performs double precision W(i)=W(i)*(B(i)/y(j))
 *  \param m:   (input) Number of rows    of W and B,
 *  \param n:   (input) Number of columns of W and B, and number of elements of vector y
 *  \param y:   (input) Double precision vector with the sum of H rows
 *  \param B:   (input) Double precision input matrix (1D column-major)
 *  \param W:   (inout) Double precision input/output matrix (1D column-major)
*/
void dupdate2W_cpu(const int m, const int n, const double *y, const double *B, double *__restrict__ W)
{
   int i;
  
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<m*n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i/m) and/or W(i) and/or B(i)=0 */
       W[i]=W[i] * (B[i] / y[i/m]);
       assert(isfinite(W[i]));
     #else
       W[i]=W[i] * (B[i] / y[i/m]);
     #endif
   }
}


/**
 *  \fn    void supdate2W_cpu(const int m, const int n, const float *y, const float *B, float *__restrict__ W)
 *  \brief This function computes simple precision W(i)=W(i)*(B(i)/y(j))
 *  \param m:   (input) Number of rows    of W and B,
 *  \param n:   (input) Number of columns of W and B, and number of elements of vector y
 *  \param y:   (input) Simple precision vector with the sum of H rows
 *  \param B:   (input) Simple precision input matrix (1D column-major)
 *  \param W:   (inout) Simple precision input/output matrix (1D column-major)
*/
void supdate2W_cpu(const int m, const int n, const float *y, const float *B, float *__restrict__ W)
{
   int i;
  
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for(i=0; i<m*n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i/m) and/or W(i) and/or B(i)=0 */
       W[i]=W[i] * (B[i] / y[i/m]);
       assert(isfinite(W[i]));
     #else
       W[i]=W[i] * (B[i] / y[i/m]);
     #endif
   }
}


//TODO documentar
void drestr1norm_cpu(const int n, const double alpha, double *__restrict__ y)
{
   int j;

   /* y =  y - alpha */
   /* y = max(y, eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
   {
     y[j] = y[j] - alpha;
     y[j] = max(y[j], dEPS);
   }
}


//TODO documentar
void srestr1norm_cpu(const int n, const float alpha, float *__restrict__ y)
{
   int j;

   /* y =  y - alpha */
   /* y = max(y, eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
   {
     y[j] = y[j] - alpha;
     y[j] = max(y[j], sEPS);
   }
}


//TODO documentar
void drestr1normSub_cpu(const int m, const int n, const double alpha, double *__restrict__ y, const int ldy)
{
   int i, j;

   /* y =  y - alpha */
   /* y = max(y, eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
     for (i=0; i<m; i++)
     {
       y[i+j*ldy] = y[i+j*ldy] - alpha;
       y[i+j*ldy] = max(y[i+j*ldy], dEPS);
     }
}


//TODO documentar
void srestr1normSub_cpu(const int m,const int n, const float alpha, float *__restrict__ y, const int ldy)
{
   int i, j;

   /* y =  y -alpha */
   /* y = max(y,eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
     for (i=0; i<m; i++)
     {
       y[i+j*ldy] = y[i+j*ldy] - alpha;
       y[i+j*ldy] = max(y[i+j*ldy], sEPS);
     }
}


//TODO documentar
void drestr2norm_cpu(const int n, const double alpha, double* M, double *__restrict__ y )
{
   int j;
   double Aux;
    
   /* y =  y - alphaH .* M  */
   /* y = max(y,eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
   {
     Aux = alpha * M[j];
     y[j] = y[j] - Aux;
     y[j] = max(y[j], dEPS);
   }
}


//TODO documentar
void srestr2norm_cpu(const int n, const float alpha, float* M, float *__restrict__ y )
{
   int j;
   float Aux;
    
   /* y =  y - alphaH .* M  */
   /* y = max(y,eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
   {
     Aux = alpha * M[j];
     y[j] = y[j] - Aux;
     y[j] = max(y[j], sEPS);
   }
}


//TODO documentar
void drestr2normSub_cpu(const int m, const int n, const double alpha, double *M, const int ldM, double *__restrict__ y, const int ldy)
{
   int i,j;
   double Aux;
    
   /* y =  y - alphaH .* M  */
   /* y = max(y,eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
     for (i=0; i<m; i++)
     {
       Aux = alpha * M[i+j*ldM];
       y[i+j*ldy] = y[i+j*ldy] - Aux;
       y[i+j*ldy] = max(y[i+j*ldy], dEPS);
     }
}


//TODO documentar
void srestr2normSub_cpu(const int m, const int n, const float alpha, float *M, const int ldM, float *__restrict__ y, const int ldy)
{
   int i,j;
   float Aux;
    
   /* y =  y - alphaH .* M  */
   /* y = max(y,eps)*/
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
     for (i=0; i<m; i++)
     {
       Aux = alpha * M[i+j*ldM];
       y[i+j*ldy] = y[i+j*ldy] - Aux;
       y[i+j*ldy] = max(y[i+j*ldy], sEPS);
     }
}


/**
 *  \fn    _void derrorbd0_cpu(const int n, const double *x, double *__restrict__ y)
 *  \brief This function performs auxiliar double precision operations when error is computed
           using betadivergence error formula and beta = 0
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Double precision input vector/matrix
 *  \param y: (inout) Double precision input/output vector/matrix
*/
void derrorbd0_cpu(const int n, const double *x, double *__restrict__ y)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i) and/or x(i)=0 */
       y[i]=(x[i]/y[i]) - log(x[i]/y[i]) - 1.0;
       assert(isfinite(y[i]));
     #else
       y[i]=(x[i]/y[i]) - log(x[i]/y[i]) - 1.0;
     #endif
   }
}


/**
 *  \fn    void serrorbd0_cpu(const int n, const float *x, float *__restrict__ y)
 *  \brief This function performs auxiliar simple precision operations when error is computed
           using betadivergence error formula and beta = 0
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Simple precision input vector/matrix
 *  \param y: (inout) Simple precision input/output vector/matrix
*/
void serrorbd0_cpu(const int n, const float *x, float *__restrict__ y)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i) and/or x(i)=0 */
       y[i]=(x[i]/y[i]) - logf(x[i]/y[i]) - 1.0f;
       assert(isfinite(y[i]));
     #else
       y[i]=(x[i]/y[i]) - logf(x[i]/y[i]) - 1.0f;
     #endif
   }
}


/**
 *  \fn    void derrorbd1_cpu(const int n, const double *x, double *__restrict__ y)
 *  \brief This function performs auxiliar double precision operations when error is computed
           using betadivergence error formula and beta = 1
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Double precision input vector/matrix
 *  \param y: (inout) Double precision input/output vector/matrix
*/
void derrorbd1_cpu(const int n, const double *x, double *__restrict__ y)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i) and/or x(i)=0 */
       y[i]=(x[i]*log(x[i]/y[i])) + y[i] - x[i];
       assert(isfinite(y[i]));
     #else
       y[i]=(x[i]*log(x[i]/y[i])) + y[i] - x[i];
     #endif
   }
}


/**
 *  \fn    void serrorbd1_cpu(const int n, const float *x, float *__restrict__ y)
 *  \brief This function performs auxiliar simple precision operations when error is computed
           using betadivergence error formula and beta = 1
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Simple precision input vector/matrix
 *  \param y: (inout) Simple precision input/output vector/matrix
*/
void serrorbd1_cpu(const int n, const float *x, float *__restrict__ y)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++)
   {
     #ifdef With_Check
       /* Here we can have NaN and Inf if y(i) and/or x(i)=0 */
       y[i]=(x[i]*logf(x[i]/y[i])) + y[i] - x[i];
       assert(isfinite(y[i]));
     #else
       y[i]=(x[i]*logf(x[i]/y[i])) + y[i] - x[i];
     #endif
   }
}


/**
 *  \fn    void derrorbdg_cpu(const int n, const double *x, double *__restrict__ y, const double beta)
 *  \brief This function performs auxiliar double precision operations when error is computed
           using betadivergence error formula with (beta != 0) and (beta != 1)
 *  \param n:    (input) Number of elements of x and y
 *  \param x:    (input) Double precision input vector/matrix
 *  \param y:    (inout) Double precision input/output vector/matrix
 *  \param beta: (input) Double precision value of beta
*/
void derrorbdg_cpu(const int n, const double *x, double *__restrict__ y, const double beta)
{
   int i;
   double dbeta, dtmp;

   dbeta=beta-1.0;
   dtmp =beta*dbeta;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++)
   {
     #ifdef With_Check
       y[i]=(pow(x[i],beta) + (dbeta*pow(y[i],beta)) - (beta*x[i]*pow(y[i],dbeta))) / dtmp;
       assert(isfinite(y[i]));
     #else
       y[i]=(pow(x[i],beta) + (dbeta*pow(y[i],beta)) - (beta*x[i]*pow(y[i],dbeta))) / dtmp;
     #endif
   }
}


/**
 *  \fn    void serrorbdg_cpu(const int n, const float *x, float *__restrict__ y, const float beta)
 *  \brief This function performs auxiliar simple precision operations when error is computed
           using betadivergence error formula with (beta != 0) and (beta != 1)
 *  \param n:    (input) Number of elements of x and y
 *  \param x:    (input) Simple precision input vector/matrix
 *  \param y:    (inout) Simple precision input/output vector/matrix
 *  \param beta: (input) Simple precision value of beta
*/
void serrorbdg_cpu(const int n, const float *x, float *__restrict__ y, const float beta)
{
   int i;
   float fbeta, ftmp;

   fbeta=beta-1.0f;
   ftmp =beta*fbeta;

   #ifdef With_ICC
     #pragma loop_count min=16
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++)
   {
     #ifdef With_Check
       y[i]=(powf(x[i],beta) + (fbeta*powf(y[i],beta)) - (beta*x[i]*powf(y[i],fbeta))) / ftmp;
       assert(isfinite(y[i]));
     #else
       y[i]=(powf(x[i],beta) + (fbeta*powf(y[i],beta)) - (beta*x[i]*powf(y[i],fbeta))) / ftmp;
     #endif
   }
}


/**
 *  \fn    void derrorbd_cpu(const int m, const int n, const int k, const double *A, const double *W, const double *H, const double beta)
 *  \brief This function returns double precision error when error is computed using betadivergence error formula
 *  \param m:    (input) Number of rows of A and W
 *  \param n:    (input) Number of columns of A and H
 *  \param k:    (input) Number of columns/rows of W/H  
 *  \param A:    (input) Double precision input matrix A
 *  \param W:    (input) Double precision input matrix W
 *  \param H:    (input) Double precision input matrix H
 *  \param beta: (input) Double precision beta value
*/
double derrorbd_cpu(const int m, const int n, const int k, const double *A, const double *W, const double *H, const double beta)
{
   double error=0.0, *tmp=NULL;

   tmp=(double *)nmf_alloc(m * n * sizeof *tmp, WRDLEN);

   cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, tmp, m);

   if (beta>=0.0 && beta<=0.0)
     derrorbd0_cpu(m*n, A, tmp);
   else
   {
     if (beta>=1.0 && beta<=1.0)
       derrorbd1_cpu(m*n, A, tmp);
     else
       derrorbdg_cpu(m*n, A, tmp, beta);
   }

   error=cblas_dasum(m*n, tmp, 1);
  
   error=sqrt((2.0*error)/((double)m*n));

   nmf_free(tmp);

   return error;
}


/**
 *  \fn    void serrorbd_cpu(const int m, const int n, const int k, const float *A, const float *W, const float *H, const float beta)
 *  \brief This function returns simple precision error when error is computed using betadivergence error formula
 *  \param m:    (input) Number of rows of A and W
 *  \param n:    (input) Number of columns of A and H
 *  \param k:    (input) Number of columns/rows of W/H  
 *  \param A:    (input) Simple precision input matrix A
 *  \param W:    (input) Simple precision input matrix W
 *  \param H:    (input) Simple precision input matrix H
 *  \param beta: (input) Simple precision beta value
*/
float serrorbd_cpu(const int m, const int n, const int k, const float *A, const float *W, const float *H, const float beta)
{
   float error=0.0, *tmp=NULL;

   tmp=(float *)nmf_alloc(m * n * sizeof *tmp, WRDLEN);

   cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, tmp, m);

   if (beta>=0.0 && beta<=0.0)
     serrorbd0_cpu(m*n, A, tmp);
   else
   {
     if (beta>=1.0 && beta<=1.0)
       serrorbd1_cpu(m*n, A, tmp);
     else
       serrorbdg_cpu(m*n, A, tmp, beta);
   }

   error=cblas_sasum(m*n, tmp, 1);

   error=sqrtf((2.0f*error)/((float)m*n));

   nmf_free(tmp);

   return error;
}
