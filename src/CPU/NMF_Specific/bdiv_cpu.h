/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    bdiv_cpu.h
 *  \brief   Header file for using the betadivergence cuda functions with CPU
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#ifndef BDIV_CPU_H
#define BDIV_CPU_H

#include <utils_cpu.h>
#include "mlsa_cpu.h"
#include "specific_cpu.h"// Public interface of the library

/* inner functions */
/* general cases   */
int dbdivg_cpu(const int, const int, const int, const double *, double *, double *, const double, const int);
int sbdivg_cpu(const int, const int, const int, const float  *, float  *, float  *, const float , const int);

int dbdivgRestrict_cpu(const int, const int,    const int,    const double *, double *, double *, const double,
                       const int, const double, const double, const unsigned short); 
int sbdivgRestrict_cpu(const int, const int,    const int,    const float *,  float *,  float *,  const float,
                       const int, const float,  const float,  const unsigned short);

/* beta=2 */
int dmlsaRestrict_cpu(const int, const int,    const int,    const double *, double *, double *,
                      const int, const double, const double, const unsigned short); 
int smlsaRestrict_cpu(const int, const int,    const int,    const float *,  float *,  float *,
                      const int, const float,  const float,  const unsigned short);

/* beta=1 */
int dbdivone_cpu(const int, const int, const int, const double *, double *, double *, const int);
int sbdivone_cpu(const int, const int, const int, const float  *, float  *, float  *, const int);

int dbdivoneRestrict_cpu(const int, const int,    const int,    const double *, double *, double *, 
                         const int, const double, const double, const unsigned short); 
int sbdivoneRestrict_cpu(const int, const int,    const int,    const float *,  float *,  float *,
                         const int, const float,  const float,  const unsigned short);

/* Utility functions*/
void dupdate1H_cpu(const int, const double *, double *__restrict__);
void supdate1H_cpu(const int, const float  *, float  *__restrict__);

void dupdate1W_cpu(const int, const int, const double *, double *__restrict__ W);
void supdate1W_cpu(const int, const int, const float  *, float  *__restrict__ W);

void dkernelH_cpu (const int, const int, const double *, const double *, double *__restrict__, const double);
void skernelH_cpu (const int, const int, const float  *, const float  *, float  *__restrict__, const float);

void dkernelW_cpu (const int, const int, const double *, const double *, double *__restrict__, const double);
void skernelW_cpu (const int, const int, const float  *, const float  *, float  *__restrict__, const float);

void dupdate2H_cpu(const int, const int, const double *, const double *, double *__restrict__);
void supdate2H_cpu(const int, const int, const float  *, const float  *, float  *__restrict__);

void dupdate2W_cpu(const int, const int, const double *, const double *, double *__restrict__);
void supdate2W_cpu(const int, const int, const float  *, const float  *, float  *__restrict__);

/*restriction functions*/
void drestr1norm_cpu(const int, const double, double *__restrict__);
void srestr1norm_cpu(const int, const float,  float  *__restrict__ );

void drestr1normSub_cpu(const int, const int, const double, double *__restrict__, const int);
void srestr1normSub_cpu(const int, const int, const float,  float  *__restrict__, const int);

void drestr2norm_cpu(const int, const double, double *, double *__restrict__);
void srestr2norm_cpu(const int, const float,  float *,  float  *__restrict__);

void drestr2normSub_cpu(const int, const int, const double, double *, const int, double *__restrict__, const int);
void srestr2normSub_cpu(const int, const int, const float,  float *,  const int, float  *__restrict__, const int); 

/* Error measurement functions */
void derrorbd0_cpu(const int, const double *, double *__restrict__);
void serrorbd0_cpu(const int, const float  *, float  *__restrict__);

void derrorbd1_cpu(const int, const double *, double *__restrict__);
void serrorbd1_cpu(const int, const float  *, float  *__restrict__);

void derrorbdg_cpu(const int, const double *, double *__restrict__, const double);
void serrorbdg_cpu(const int, const float  *, float  *__restrict__, const float);

#endif
