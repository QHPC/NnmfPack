/**
 *  \file    asna_cpu.c
 *  \brief   File with functions to use the ASNA algorithm to solve the NNLS problem with multiple rigth hand sides using the Kullback-Leibler divergence as error measure for CPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/

#include "asna_cpu.h"

/**
 *  \fn   int dasna_cpu(const int f, const int n, const int o, const double *Xini, const double *Bini, double *W ,const int iter)
 *  \brief This function performs a matrix nonnegative approximation using Active-Set Newton Algorithm (ASNA) X = BW using parallel computation techniques
 *  \param f: (input) Number of features
 *  \param n: (input) Number of dictionary atoms
 *  \param o: (input) Number of observations
 *  \param Xini: (input) Double precision matrix to factorize (f x o)(1D column-major)
 *  \param Bini: (input) Double precision dictionary matrix (f x n) (1D column-major)
 *  \param W:   (output) Double precision weigths matrix (n x o) (1D column-major)
 *  \param iter: (input) Maximum number of iterations to perform
 *  \param nnz:  (input) Maximum number of active atoms per observation (to restrict memory size of the hessian matrix, maximum n)
*/
int dasna_cpu(const int f, const int n, const int o, const double *Xini, const double *Bini, double *W ,const int iter, const int nnz)
{
   double stepSize, num, norm;
   int    i, j, k, it, info, gPtr, nThreads, tid, gradientComputed, minIdx, converged, nObs;
   double *B=NULL, *Bt=NULL,  *Hwa=NULL,  *HwaS=NULL, *normsB=NULL, *maxStep=NULL, *gradAll2=NULL;
   double *X=NULL, *R=NULL,   *Mult=NULL, *grad=NULL, *gradS=NULL,  *normsX=NULL,  *initWeights=NULL;
   double *V=NULL, *BtO=NULL, *Aux=NULL,  *AuxS=NULL, *MultS=NULL,  *gradAll=NULL, *maxStepS=NULL;

   atom *atomPtr=NULL, *atomPtr2=NULL;
   int  *initAtoms=NULL;
   observation *activeObs=NULL;

   B          =(double *)     nmf_alloc(f * n * sizeof *B,           WRDLEN);
   R          =(double *)     nmf_alloc(f * o * sizeof *R,           WRDLEN);
   X          =(double *)     nmf_alloc(f * o * sizeof *X,           WRDLEN);
   V          =(double *)     nmf_alloc(f * o * sizeof *V,           WRDLEN);
   Bt         =(double *)     nmf_alloc(n * f * sizeof *Bt,          WRDLEN);
   BtO        =(double *)     nmf_alloc(n * o * sizeof *BtO,         WRDLEN);
   normsX     =(double *)     nmf_alloc(o     * sizeof *normsX,      WRDLEN);
   normsB     =(double *)     nmf_alloc(n     * sizeof *normsB,      WRDLEN);
   gradAll    =(double *)     nmf_alloc(n * o * sizeof *gradAll,     WRDLEN);
   gradAll2   =(double *)     nmf_alloc(n * o * sizeof *gradAll2,    WRDLEN);
   initWeights=(double *)     nmf_alloc(o     * sizeof *initWeights, WRDLEN);
   initAtoms  =(int *)        nmf_alloc(o     * sizeof *initAtoms,   WRDLEN);
   activeObs  =(observation *)nmf_alloc(o     * sizeof *activeObs,   WRDLEN);

   if (B==NULL || R==NULL || X==NULL || V==NULL || Bt==NULL || BtO==NULL) { return -1; }
   if (normsX==NULL || normsB ==NULL || gradAll==NULL || gradAll2==NULL)  { return -1; }
   if (initWeights==NULL || initAtoms==NULL || activeObs==NULL)           { return -1; }

   //allocation for false sharing private thread variables
   nThreads=omp_get_max_threads();
   MultS   =(double *)nmf_alloc(nThreads * f         * sizeof *MultS,    WRDLEN);
   AuxS    =(double *)nmf_alloc(nThreads * f         * sizeof *AuxS,     WRDLEN);
   gradS   =(double *)nmf_alloc(nThreads * n         * sizeof *gradS,    WRDLEN);
   maxStepS=(double *)nmf_alloc(nThreads * n         * sizeof *maxStepS, WRDLEN);
   HwaS    =(double *)nmf_alloc(nThreads * nnz * nnz * sizeof *HwaS,     WRDLEN);

   if (MultS==NULL || AuxS==NULL || gradS==NULL || maxStepS==NULL) { return -1; }
   if (HwaS == NULL) { printf("Error allocating the hessian matrix\n"); return -1; }

   //init observation structure
   nObs = o;
   for(i=0; i<o; i++)
   {
     activeObs[i].obsIdx=i;
     activeObs[i].converged=0;
     activeObs[i].nAtoms=0;
     activeObs[i].firstAtom=NULL;
     activeObs[i].lastAtom=NULL;
   }

   //Normalize input matrices
   cblas_dcopy  (f*o, Xini, 1, X, 1);
   dnrm1ColsSave(f, o, X, normsX);
   cblas_dcopy  (f*n, Bini, 1, B, 1);
   dnrm2ColsSave(f, n, B, normsB);
	
   daddEsc_cpu(f*o, dEPS, X);

   //precomputing Bt and fixed part of eq. 11
   dtrans_cpu(f, n, B, Bt);
   dmultOnes (f, n, o, Bt, BtO);
	
   //Computing initial atoms and adding them to the active set
   if (dInitAtoms(f, n, o, X, B, initAtoms, initWeights) < 0) {return -1; }

   #pragma omp parallel for schedule(static)
   for(j=0; j<o; j++)
   {
     addAtom(&activeObs[j], initAtoms[j]);
     W[initAtoms[j]+ j*n] = initWeights[j];
   }

   //Perform algoritm iterations (starts with 2 for equivalence with the original version)		
   for(it=2; it<iter; it++)
   {
     //V = activeB * activeW
     #pragma omp parallel for  schedule(dynamic) private(i, k, atomPtr)
     for(j=0; j<nObs; j++)
     {
       if(!activeObs[j].converged)
       {
         for(i=0; i<f; i++)
         {
           V[i + j*f] =0;
           for(k=0, atomPtr=activeObs[j].firstAtom; k<activeObs[j].nAtoms; k++)
           {
             V[i + j*f] += B[i + atomPtr->atomIdx * f] * W[ atomPtr->atomIdx + j*n];
             if (k!=activeObs[j].nAtoms - 1)
               atomPtr = atomPtr->nextAtom;
           }
         }
       }
     }

     //R = X/V
     ddiv_cpu(f*o, X, V, R);
		
     //Each 2 iterations try to add new atoms
     if (it%2 == 0)
     {
       cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n, o, f, 1, Bt, n, R, f, 0, gradAll, n);

       //Try to change dgemm+dsub with dcopy+dgemm
       dsub_cpu(n*o, BtO, gradAll, gradAll);

       gradientComputed=1;

       //Try to remove this dcopy
       cblas_dcopy(n*o, gradAll, 1, gradAll2, 1);
			
       //Check convergence
       if (it%10 == 0)
       {
         converged = 0;
         #pragma omp parallel for schedule(static)
         for(i=0; i<n*o; i++)
           if(gradAll2[i] > 0) gradAll2[i]=0;
				
         //Mark each converged observation
         for(j=0; j<o; j++)
         {
           if (!activeObs[j].converged)
           {
             norm = cblas_ddot(n, &gradAll2[j*n], 1, &gradAll2[j*n], 1);
             if (norm < dTOL)
             {
               activeObs[j].converged = 1;
               converged++;
             }
           }
           else converged++;
         }				

         //If all observations are converged finish the algorithm
         if (converged == o)
         {
           //rescale back to original scale of S
           freeObservations(nObs, activeObs);
           drowsRescale(n, o, W, normsB);
           dcolsRescale(n, o, W, normsX);

           nmf_free(X); nmf_free(V); nmf_free(Bt);  nmf_free(HwaS); nmf_free(MultS); nmf_free(normsX);
           nmf_free(B); nmf_free(R); nmf_free(BtO); nmf_free(AuxS); nmf_free(gradS); nmf_free(normsB);
           
           nmf_free(gradAll);  nmf_free(gradAll2);  nmf_free(activeObs);
           nmf_free(maxStepS); nmf_free(initAtoms); nmf_free(initWeights);

           return 0;
         }
       } // end if(it%10)

       //add only entries that are not yet active
       #pragma omp parallel for schedule(dynamic) private(i,atomPtr)
       for(j=0; j<nObs; j++)
       {
         if (!activeObs[j].converged)
         {
           for(i=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms;i++)
           {
             gradAll2[atomPtr->atomIdx + j*n] = 0;
             if (i != activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
           }
         }
       }

       //Adding atoms
       #pragma omp parallel for schedule(dynamic) private(minIdx)
       for(j=0; j<nObs; j++)
       {
         if (!activeObs[j].converged)
         {
           minIdx = Idmin_cpu(n, &gradAll2[j*n]);
           if(gradAll2[minIdx + j *n] < - dGRA)
           {
             addAtom(&activeObs[j], minIdx);
             W[minIdx + j*n] = gradAll2[minIdx + j *n] < dTOL? dTOL: gradAll2[minIdx + j *n]; 
           }
         }
       }
     } // end then of if(it%2 == 0)
     else
       gradientComputed = 0;
		
     //Computing each non-converged observation in parallel
     #pragma omp parallel default(none) shared(nObs,activeObs,B,R,gradientComputed,gradAll,BtO,V,it,W, MultS,AuxS, maxStepS, gradS,HwaS,nThreads) private(i,atomPtr,Mult, grad,Aux,k,atomPtr2,Hwa,info,maxStep,minIdx,stepSize,gPtr,num,tid)
     {
       #ifndef With_MKL
         int rank=nnz, rani=1, ranj=n;
         char Low='L';
       #endif
       tid     = omp_get_thread_num();
       Mult    = &MultS[tid * f];
       Aux     = &AuxS[tid * f];
       maxStep = &maxStepS[tid * n];
       grad    = &gradS[tid * n];
       Hwa     = &HwaS[(unsigned long)tid * nnz * nnz];

       #pragma omp for schedule(dynamic)
       for(j=0; j<nObs; j++)
       {

         if (!activeObs[j].converged)
         {
           //Compute or copy gradients and the hessian matrix
           for(i=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms; i++)
           {
             dmult_cpu(f, &B[atomPtr->atomIdx*f], &R[j*f], Mult);

             if (gradientComputed)
               grad[i] = gradAll[atomPtr->atomIdx + j*n];
             else
               grad[i] = BtO[atomPtr->atomIdx + j*n] - cblas_dasum(f, Mult, 1);

             ddiv_cpu(f, Mult, &V[j*f], Aux);

             for(k=i, atomPtr2=atomPtr; k<activeObs[j].nAtoms; k++ )
             {
               Hwa[ k + i * nnz] = cblas_ddot(f, &B[atomPtr2->atomIdx *f], 1, Aux, 1);

               if(k==i) Hwa[k + i*nnz] += dGRA; 

               if(k!= activeObs[j].nAtoms - 1) atomPtr2 = atomPtr2->nextAtom;
             }

             if(i!= activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
           }

           //Solve Hwa * x = grad;
           #ifdef With_MKL
             info = LAPACKE_dpotrf(LAPACK_COL_MAJOR, 'L', activeObs[j].nAtoms, Hwa, nnz);
             if (info != 0) printf("Error en lapack(dpotrf): %d it=%d, obs=%d\n", info, it, j);

             info = LAPACKE_dpotrs(LAPACK_COL_MAJOR,'L', activeObs[j].nAtoms, 1, Hwa, nnz, grad, n);
             if (info != 0) printf("Error en lapack(dpotrs): %d\n", info);
           #else
             dpotrf_(&Low, &activeObs[j].nAtoms, Hwa, &rank, &info);
             if (info != 0) printf("Error en lapack(dpotrf): %d it=%d, obs=%d\n", info, it, j);

             dpotrs_(&Low, &activeObs[j].nAtoms, &rani, Hwa, &rank, grad, &ranj, &info);
             if (info != 0) printf("Error en lapack(dpotrs): %d\n", info);
           #endif

           //Computing step direction
           for(i=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms; i++)
           {
             if(grad[i]<=0)
               maxStep[i] = HUGE_VAL;
             else
               maxStep[i] = W[atomPtr->atomIdx + n*j] / grad[i];

             if(i!= activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
           }

           //Computing step size
           minIdx = Idmin_cpu(activeObs[j].nAtoms, maxStep);
           stepSize = maxStep[minIdx];
           if(stepSize<0 || stepSize>1) stepSize = 1;

           //Update weigths and remove negative or zero weigths from the active set 
           for(i=0,gPtr=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms; i++, gPtr++)
           {
             num = W[atomPtr->atomIdx + n*j] - stepSize * grad[gPtr];

             if(num <= dEPS) 
             {
               W[atomPtr->atomIdx + n*j] = 0;
               if(i!=activeObs[j].nAtoms - 1)
               {
                 atomPtr2 = atomPtr->nextAtom;
                 removeAtom(&activeObs[j], atomPtr->atomIdx);
                 atomPtr = atomPtr2;
                 i--;
               }
               else
                 removeAtom(&activeObs[j], atomPtr->atomIdx);
             }
             else
             {
               W[atomPtr->atomIdx + n*j] = num;
               if (i!= activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
             }
           }
         } // end if(!activeObs[j].converged)
       } // end for(j)
     } // end pragma parallel 
   } // end for(it=2)

   freeObservations(nObs,activeObs);

   drowsRescale(n, o, W, normsB);
   dcolsRescale(n, o, W, normsX);

   nmf_free(X); nmf_free(V); nmf_free(Bt);  nmf_free(HwaS); nmf_free(MultS); nmf_free(normsX); nmf_free(gradAll);
   nmf_free(B); nmf_free(R); nmf_free(BtO); nmf_free(AuxS); nmf_free(gradS); nmf_free(normsB); nmf_free(gradAll2);
           
   nmf_free(activeObs); nmf_free(maxStepS); nmf_free(initAtoms); nmf_free(initWeights);

   return 0;
}


/**
 *  \fn   int sasna_cpu(const int f, const int n, const int o, const float *Xini, const float *Bini, float *W ,const int iter)
 *  \brief This function performs a matrix nonnegative approximation using Active-Set Newton Algorithm (ASNA) X = BW using parallel computation techniques
 *  \param f: (input) Number of features
 *  \param n: (input) Number of dictionary atoms
 *  \param o: (input) Number of observations
 *  \param Xini: (input) Simple precision matrix to factorize (f x o)(1D column-major)
 *  \param Bini: (input) Simple precision dictionary matrix (f x n) (1D column-major)
 *  \param W:   (output) Simple precision weigths matrix (n x o) (1D column-major)
 *  \param iter: (input) Maximum number of iterations to perform
 *  \param nnz:  (input) Maximum number of active atoms per observation (to restrict memory size of the hessian matrix, maximum n)
*/
int sasna_cpu(const int f, const int n, const int o, const float *Xini, const float *Bini, float *W ,const int iter, const int nnz)
{
   float stepSize, num, norm;
   int   i, j, k, it, info, gPtr, nThreads, tid, gradientComputed, minIdx, converged, nObs;
   float *B=NULL, *Bt=NULL,  *Hwa=NULL,  *HwaS=NULL, *normsB=NULL, *maxStep=NULL, *gradAll2=NULL;
   float *X=NULL, *R=NULL,   *Mult=NULL, *grad=NULL, *gradS=NULL,  *normsX=NULL,  *initWeights=NULL;
   float *V=NULL, *BtO=NULL, *Aux=NULL,  *AuxS=NULL, *MultS=NULL,  *gradAll=NULL, *maxStepS=NULL;

   atom *atomPtr=NULL, *atomPtr2=NULL;
   int  *initAtoms=NULL;
   observation *activeObs=NULL;

   B          =(float *)      nmf_alloc(f * n * sizeof *B,           WRDLEN);
   R          =(float *)      nmf_alloc(f * o * sizeof *R,           WRDLEN);
   X          =(float *)      nmf_alloc(f * o * sizeof *X,           WRDLEN);
   V          =(float *)      nmf_alloc(f * o * sizeof *V,           WRDLEN);
   Bt         =(float *)      nmf_alloc(n * f * sizeof *Bt,          WRDLEN);
   BtO        =(float *)      nmf_alloc(n * o * sizeof *BtO,         WRDLEN);
   normsX     =(float *)      nmf_alloc(o     * sizeof *normsX,      WRDLEN);
   normsB     =(float *)      nmf_alloc(n     * sizeof *normsB,      WRDLEN);
   gradAll    =(float *)      nmf_alloc(n * o * sizeof *gradAll,     WRDLEN);
   gradAll2   =(float *)      nmf_alloc(n * o * sizeof *gradAll2,    WRDLEN);
   initWeights=(float *)      nmf_alloc(o     * sizeof *initWeights, WRDLEN);
   initAtoms  =(int *)        nmf_alloc(o     * sizeof *initAtoms,   WRDLEN);
   activeObs  =(observation *)nmf_alloc(o     * sizeof *activeObs,   WRDLEN);

   if (B==NULL || R==NULL || X==NULL || V==NULL || Bt==NULL || BtO==NULL) { return -1; }
   if (normsX==NULL || normsB ==NULL || gradAll==NULL || gradAll2==NULL)  { return -1; }
   if (initWeights==NULL || initAtoms==NULL || activeObs==NULL)           { return -1; }

   //allocation for false sharing private thread variables
   nThreads=omp_get_max_threads();
   MultS   =(float *)nmf_alloc(nThreads * f         * sizeof *MultS,    WRDLEN);
   AuxS    =(float *)nmf_alloc(nThreads * f         * sizeof *AuxS,     WRDLEN);
   gradS   =(float *)nmf_alloc(nThreads * n         * sizeof *gradS,    WRDLEN);
   maxStepS=(float *)nmf_alloc(nThreads * n         * sizeof *maxStepS, WRDLEN);
   HwaS    =(float *)nmf_alloc(nThreads * nnz * nnz * sizeof *HwaS,     WRDLEN);

   if (MultS==NULL || AuxS==NULL || gradS==NULL || maxStepS==NULL) { return -1; }
   if (HwaS == NULL) { printf("Error allocating the hessian matrix\n"); return -1; }

   //init observation structure
   nObs = o;
   for(i=0; i<o; i++)
   {
     activeObs[i].obsIdx=i;
     activeObs[i].converged=0;
     activeObs[i].nAtoms=0;
     activeObs[i].firstAtom=NULL;
     activeObs[i].lastAtom=NULL;
   }

   //Normalize input matrices
   cblas_scopy  (f*o, Xini, 1, X, 1);
   snrm1ColsSave(f, o, X, normsX);
   cblas_scopy  (f*n, Bini, 1, B, 1);
   snrm2ColsSave(f, n, B, normsB);
	
   saddEsc_cpu(f*o, sEPS, X);

   //precomputing Bt and fixed part of eq. 11
   strans_cpu(f, n, B, Bt);
   smultOnes (f, n, o, Bt, BtO);
	
   //Computing initial atoms and adding them to the active set
   if (sInitAtoms(f, n, o, X, B, initAtoms, initWeights) < 0) {return -1; }

   #pragma omp parallel for schedule(static)
   for(j=0; j<o; j++)
   {
     addAtom(&activeObs[j], initAtoms[j]);
     W[initAtoms[j]+ j*n] = initWeights[j];
   }

   //Perform algoritm iterations (starts with 2 for equivalence with the original version)		
   for(it=2; it<iter; it++)
   {
     //V = activeB * activeW
     #pragma omp parallel for  schedule(dynamic) private(i,k,atomPtr)
     for(j=0; j<nObs; j++)
     {
       if (!activeObs[j].converged)
       {
         for(i=0; i<f; i++)
         {
           V[i + j*f] =0;
           for(k=0, atomPtr=activeObs[j].firstAtom; k<activeObs[j].nAtoms; k++)
           {
             V[i + j*f] += B[i + atomPtr->atomIdx * f] * W[ atomPtr->atomIdx + j*n];
             if (k!=activeObs[j].nAtoms - 1)
               atomPtr = atomPtr->nextAtom;
           }
         }
       }
     }

     //R = X/V
     sdiv_cpu(f*o, X, V, R);
		
     //Each 2 iterations try to add new atoms
     if (it%2 == 0)
     {
       cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n, o, f, 1, Bt, n, R, f, 0, gradAll, n);

       //Try to change sgemm+ssub with scopy+sgemm
       ssub_cpu(n*o, BtO, gradAll, gradAll);

       gradientComputed=1;

       //Try to remove this scopy
       cblas_scopy(n*o, gradAll, 1, gradAll2, 1);
			
       //Check convergence
       if (it%10 == 0)
       {
         converged = 0;
         #pragma omp parallel for schedule(static)
         for(i=0; i<n*o; i++)
           if(gradAll2[i] > 0) gradAll2[i]=0;
				
         //Mark each converged observation
         for(j=0; j<o; j++)
         {
           if (!activeObs[j].converged)
           {
             norm = cblas_sdot(n, &gradAll2[j*n], 1, &gradAll2[j*n], 1);
             if(norm < sTOL)
             {
               activeObs[j].converged = 1;
               converged++;
             }
           }
           else converged++;
         }				

         //If all observations are converged finish the algorithm
         if (converged == o)
         {
           //rescale back to original scale of S
           freeObservations(nObs, activeObs);
           srowsRescale(n, o, W, normsB);
           scolsRescale(n, o, W, normsX);

           nmf_free(X); nmf_free(V); nmf_free(Bt);  nmf_free(HwaS); nmf_free(MultS); nmf_free(normsX);
           nmf_free(B); nmf_free(R); nmf_free(BtO); nmf_free(AuxS); nmf_free(gradS); nmf_free(normsB);
           
           nmf_free(gradAll);  nmf_free(gradAll2);  nmf_free(activeObs);
           nmf_free(maxStepS); nmf_free(initAtoms); nmf_free(initWeights);

           return 0;
         }
       } // end if(it%10)

       //add only entries that are not yet active
       #pragma omp parallel for schedule(dynamic) private(i,atomPtr)
       for(j=0; j<nObs; j++)
       {
         if (!activeObs[j].converged)
         {
           for(i=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms;i++)
           {
             gradAll2[atomPtr->atomIdx + j*n] = 0;
             if (i != activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
           }
         }
       }

       //Adding atoms
       #pragma omp parallel for schedule(dynamic) private(minIdx)
       for(j=0; j<nObs; j++)
       {
         if (!activeObs[j].converged)
         {
           minIdx = Ismin_cpu(n, &gradAll2[j*n]);
           if(gradAll2[minIdx + j *n] < - sGRA)
           {
             addAtom(&activeObs[j], minIdx);
             W[minIdx + j*n] = gradAll2[minIdx + j *n] < sTOL? sTOL: gradAll2[minIdx + j *n]; 
           }
         }
       }
     } // end then of if(it%2 == 0)
     else
       gradientComputed = 0;
		
     //Computing each non-converged observation in parallel
     #pragma omp parallel default(none) shared(nObs,activeObs,B,R,gradientComputed,gradAll,BtO,V,it,W, MultS,AuxS, maxStepS, gradS,HwaS,nThreads) private(i,atomPtr,Mult, grad,Aux,k,atomPtr2,Hwa,info,maxStep,minIdx,stepSize,gPtr,num,tid)
     {
       #ifndef With_MKL
         int rank=nnz, rani=1, ranj=n;
         char Low='L';
       #endif
       tid     = omp_get_thread_num();
       Mult    = &MultS[tid * f];
       Aux     = &AuxS[tid * f];
       maxStep = &maxStepS[tid * n];
       grad    = &gradS[tid * n];
       Hwa     = &HwaS[(unsigned long)tid * nnz * nnz];

       #pragma omp for schedule(dynamic)
       for(j=0; j<nObs; j++)
       {
         if (!activeObs[j].converged)
         {
           //Compute or copy gradients and the hessian matrix
           for(i=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms; i++)
           {
             smult_cpu(f, &B[atomPtr->atomIdx*f], &R[j*f], Mult);

             if (gradientComputed)
               grad[i] = gradAll[atomPtr->atomIdx + j*n];
             else
               grad[i] = BtO[atomPtr->atomIdx + j*n] - cblas_sasum(f, Mult, 1);

             sdiv_cpu(f, Mult, &V[j*f], Aux);

             for(k=i, atomPtr2=atomPtr; k<activeObs[j].nAtoms; k++ )
             {
               Hwa[ k + i * nnz] = cblas_sdot(f, &B[atomPtr2->atomIdx *f], 1, Aux, 1);

               if(k==i) Hwa[k + i*nnz] += sGRA; 

               if(k!= activeObs[j].nAtoms - 1) atomPtr2 = atomPtr2->nextAtom;
             }

             if(i!= activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
           }

           //Solve Hwa * x = grad;
           #ifdef With_MKL
             info = LAPACKE_spotrf(LAPACK_COL_MAJOR, 'L', activeObs[j].nAtoms, Hwa, nnz);
             if (info != 0) printf("Error en lapack(dpotrf): %d it=%d, obs=%d\n", info, it, j);

             info = LAPACKE_spotrs(LAPACK_COL_MAJOR,'L', activeObs[j].nAtoms, 1, Hwa, nnz, grad, n);
             if (info != 0) printf("Error en lapack(dpotrs): %d\n", info);
           #else
             rank=nnz; rani=1; ranj=n;
             spotrf_(&Low, &activeObs[j].nAtoms, Hwa, &rank, &info);
             if (info != 0) printf("Error en lapack(dpotrf): %d it=%d, obs=%d\n", info, it, j);

             spotrs_(&Low, &activeObs[j].nAtoms, &rani, Hwa, &rank, grad, &ranj, &info);
             if (info != 0) printf("Error en lapack(dpotrs): %d\n", info);
           #endif

           //Computing step direction
           for(i=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms; i++)
           {
             if(grad[i]<=0)
               maxStep[i] = HUGE_VAL;
             else
               maxStep[i] = W[atomPtr->atomIdx + n*j] / grad[i];

             if(i!= activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
           }

           //Computing step size
           minIdx = Ismin_cpu(activeObs[j].nAtoms, maxStep);
           stepSize = maxStep[minIdx];
           if(stepSize<0 || stepSize>1) stepSize = 1;

           //Update weigths and remove negative or zero weigths from the active set 
           for(i=0,gPtr=0, atomPtr=activeObs[j].firstAtom; i<activeObs[j].nAtoms; i++, gPtr++)
           {
             num = W[atomPtr->atomIdx + n*j] - stepSize * grad[gPtr];

             if(num <= sEPS) 
             {
               W[atomPtr->atomIdx + n*j] = 0;
               if(i!=activeObs[j].nAtoms - 1)
               {
                 atomPtr2 = atomPtr->nextAtom;
                 removeAtom(&activeObs[j], atomPtr->atomIdx);
                 atomPtr = atomPtr2;
                 i--;
               }
               else
                 removeAtom(&activeObs[j], atomPtr->atomIdx);
             }
             else
             {
               W[atomPtr->atomIdx + n*j] = num;
               if (i!= activeObs[j].nAtoms - 1) atomPtr = atomPtr->nextAtom;
             }
           }
         } // end if(!activeObs[j].converged)
       } // end for(j)
     } // end pragma parallel 
   } // end for(it=2)

   freeObservations(nObs,activeObs);

   srowsRescale(n, o, W, normsB);
   scolsRescale(n, o, W, normsX);

   nmf_free(X); nmf_free(V); nmf_free(Bt);  nmf_free(HwaS); nmf_free(MultS); nmf_free(normsX); nmf_free(gradAll);
   nmf_free(B); nmf_free(R); nmf_free(BtO); nmf_free(AuxS); nmf_free(gradS); nmf_free(normsB); nmf_free(gradAll2);
           
   nmf_free(activeObs); nmf_free(maxStepS); nmf_free(initAtoms); nmf_free(initWeights);

   return 0;
}


/**
 *  \fn    void dmultOnes(const int f, const int n, const int o, const double *M, double *__restrict__ MO)
 *  \brief This function performs the operation MO = M * ones(f,o)
 *  \param f: (input) Number of features
 *  \param n: (input) Number of dictionary atoms
 *  \param o: (input) Number of observations
 *  \param M: (input) Double precision matrix
 *  \param MO: (output) Double precision matrix
*/
void dmultOnes(const int f, const int n, const int o, const double *M, double *__restrict__ MO)
{
   double *Ones;
	
   Ones=(double *)nmf_alloc(f * o * sizeof *Ones, WRDLEN);
	
   dmemset_cpu(f*o, Ones, 1.0);
   cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n, o, f, 1, M, n, Ones, f, 0, MO, n);

   nmf_free(Ones);
}


/**
 *  \fn    void smultOnes(const int f, const int n, const int o, const float *M, double *__restrict__ MO)
 *  \brief This function performs the operation MO = M * ones(f,o)
 *  \param f: (input) Number of features
 *  \param n: (input) Number of dictionary atoms
 *  \param o: (input) Number of observations
 *  \param M: (input) Double precision matrix
 *  \param MO: (output) Double precision matrix
*/
void smultOnes(const int f, const int n, const int o, const float *M, float *__restrict__ MO)
{
   float *Ones;
	
   Ones=(float *)nmf_alloc(f * o * sizeof *Ones, WRDLEN);
	
   smemset_cpu(f*o, Ones, 1.0);
   cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n, o, f, 1, M, n, Ones, f, 0, MO, n);

   nmf_free(Ones);
}


/**
 *  \fn    int dInitlAtoms(const int f, const int n, const int o, const double *X, const double *B, int *__restrict__ initAtoms, double *__restrict__ initWeights)
 *  \brief This function performs the initialization os the ASNA algorithm comptuting 
 * 	the best weigth which minimizes alone the KL-divergence for each observation
 *  \param f: (input) Number of features
 *  \param n: (input) Number of dictionary atoms
 *  \param o: (input) Number of observations
 *  \param X: (input) Double precision matrix to factorize (f x o)(1D column-major)
 *  \param B: (input) Double precision dictionary matrix (f x n) (1D column-major)
 *  \param initAtoms: (output) Integer vector containing the index values of the initial atoms
 *  \param initAtoms: (output) Double precision vector containing the weigth for each initial atom
*/
int dInitAtoms(const int f, const int n, const int o, const double *X, const double *B, int *__restrict__ initAtoms, double *__restrict__ initWeights)
{
   int i, j, k;
   double sX;
   double *sB=NULL, *Aux=NULL, *KLDiv=NULL, *W=NULL, *logB=NULL;
	
   sB   =(double *)nmf_alloc(n     * sizeof *sB,    WRDLEN);
   Aux  =(double *)nmf_alloc(o     * sizeof *Aux,   WRDLEN);
   W    =(double *)nmf_alloc(n * o * sizeof *W,     WRDLEN);
   KLDiv=(double *)nmf_alloc(n * o * sizeof *KLDiv, WRDLEN);
   logB =(double *)nmf_alloc(n * f * sizeof *logB,  WRDLEN);

   if (sB==NULL || Aux==NULL || W==NULL || KLDiv==NULL || logB==NULL) { return -1; }

   //Compute sum(B)
   #pragma omp parallel for schedule(static)
   for(j=0; j<n; j++)
      sB[j]=cblas_dasum(f, &B[j*f], 1);
	
   //Compute W=bsxfun(@rdivide,sum(X,1), sum(B,1)');
   //KLDiv = bsxfun(@times,sum(X,1),log(A))
   #pragma omp parallel for schedule(static) private(sX, i, k)
   for(j=0; j<o; j++)
   {
      sX=cblas_dasum(f, &X[j*f], 1);
      for(i=0; i<n; i++)
      {
         W[i + j*n] = sX / sB[i] ;
         KLDiv[i + j*n] = log(W[i + j*n]) * sX;
      }		

      //Compute sum(X.*log(X));
      Aux[j] = 0;
      for(k=0; k<f; k++)
         Aux[j] = Aux[j] + X[k + j*f] * log(X[k + j*f]);

   }

   //compute log(B)
   #pragma omp parallel for  schedule(static)
   for(i=0; i<f*n; i++)
     logB[i] = log(B[i]);

   //Compute KLDiv = log(B)' * X + KLDiv
   cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, n, o, f, 1, logB, f, X, f, 1, KLDiv, n);

   //Compute KLDiv = KLDiv - expanded(Aux)
   #pragma omp parallel for  schedule(static) private(i)
   for(j=0; j<o ; j++)
     for(i=0; i<n; i++)
       KLDiv[i + j*n] = KLDiv[i + j*n] - Aux[j];

   //Look for the minimum divergence for each observation (absooute)
   #pragma omp parallel for  schedule(static)
   for(j=0; j<o; j++)
   {
     #ifdef With_MKL
        initAtoms[j]= cblas_idamin(n, &KLDiv[j*n], 1);
     #else
       initAtoms[j] = Idamin_cpu(n, &KLDiv[j*n]);
     #endif
     initWeights[j] = W[initAtoms[j] + j * n];
   }

   nmf_free(sB);
   nmf_free(Aux);
   nmf_free(logB);
   nmf_free(KLDiv);
   nmf_free(W);

   return 0;
}


/**
 *  \fn    int sInitlAtoms(const int f, const int n, const int o, const float *X, const float *B, int *__restrict__ initAtoms, float *__restrict__ initWeights)
 *  \brief This function performs the initialization os the ASNA algorithm comptuting 
 * 	the best weigth which minimizes alone the KL-divergence for each observation
 *  \param f: (input) Number of features
 *  \param n: (input) Number of dictionary atoms
 *  \param o: (input) Number of observations
 *  \param X: (input) Simple precision matrix to factorize (f x o)(1D column-major)
 *  \param B: (input) Simple precision dictionary matrix (f x n) (1D column-major)
 *  \param initAtoms: (output) Integer vector containing the index values of the initial atoms
 *  \param initAtoms: (output) Simple precision vector containing the weigth for each initial atom
*/
int sInitAtoms(const int f, const int n, const int o, const float *X, const float *B, int *__restrict__ initAtoms, float *__restrict__ initWeights)
{
   int i, j, k;
   float sX;
   float *sB=NULL, *Aux=NULL, *KLDiv=NULL, *W=NULL, *logB=NULL;
	
   sB   =(float *)nmf_alloc(n     * sizeof *sB,    WRDLEN);
   Aux  =(float *)nmf_alloc(o     * sizeof *Aux,   WRDLEN);
   W    =(float *)nmf_alloc(n * o * sizeof *W,     WRDLEN);
   KLDiv=(float *)nmf_alloc(n * o * sizeof *KLDiv, WRDLEN);
   logB =(float *)nmf_alloc(n * f * sizeof *logB,  WRDLEN);

   if (sB==NULL || Aux==NULL || W==NULL || KLDiv==NULL || logB==NULL) { return -1; }

   //Compute sum(B)
   #pragma omp parallel for schedule(static)
   for(j=0; j<n; j++)
      sB[j]=cblas_sasum(f, &B[j*f], 1);
	
   //Compute W=bsxfun(@rdivide,sum(X,1), sum(B,1)');
   //KLDiv = bsxfun(@times,sum(X,1),log(A))
   #pragma omp parallel for schedule(static) private(sX, i, k)
   for(j=0; j<o; j++)
   {
      sX=cblas_sasum(f, &X[j*f], 1);
      for(i=0; i<n; i++)
      {
         W[i + j*n] = sX / sB[i] ;
         KLDiv[i + j*n] = logf(W[i + j*n]) * sX;
      }		

      //Compute sum(X.*log(X));
      Aux[j] = 0;
      for(k=0; k<f; k++)
         Aux[j] = Aux[j] + X[k + j*f] * logf(X[k + j*f]);

   }

   //compute log(B)
   #pragma omp parallel for  schedule(static)
   for(i=0; i<f*n; i++)
     logB[i] = logf(B[i]);

   //Compute KLDiv = log(B)' * X + KLDiv
   cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, n, o, f, 1, logB, f, X, f, 1, KLDiv, n);

   //Compute KLDiv = KLDiv - expanded(Aux)
   #pragma omp parallel for  schedule(static) private(i)
   for(j=0; j<o ; j++)
     for(i=0; i<n; i++)
       KLDiv[i + j*n] = KLDiv[i + j*n] - Aux[j];

   //Look for the minimum divergence for each observation (absooute)
   #pragma omp parallel for  schedule(static)
   for(j=0; j<o; j++)
   {
     #ifdef With_MKL
        initAtoms[j]= cblas_isamin(n, &KLDiv[j*n], 1);
     #else
       initAtoms[j] = Isamin_cpu(n, &KLDiv[j*n]);
     #endif
     initWeights[j] = W[initAtoms[j] + j * n];
   }

   nmf_free(sB);
   nmf_free(Aux);
   nmf_free(logB);
   nmf_free(KLDiv);
   nmf_free(W);

   return 0;
}


/**
 *  \fn   void addAtom(observation *obs, const int newIdx)
 *  \brief This function adds an atom with index newIdx to the last position of  the  active set of observation obs
 *  \param obs: (inout) Observation structure where the atom is going to be added
 *  \param rnewIdx: (input) Index to Add
*/
void addAtom(observation *obs, const int newIdx)
{
   atom *atomPtr=NULL;
	
   if(obs->nAtoms == 0)
   {
      obs->firstAtom=(atom *)nmf_alloc(sizeof(atom), WRDLEN);
      obs->lastAtom=obs->firstAtom;
      obs->nAtoms++;
      obs->firstAtom->atomIdx = newIdx;
      obs->firstAtom->prevAtom = NULL;
      obs->firstAtom->nextAtom = NULL;
   }
   else
   {
      atomPtr =(atom *)nmf_alloc(sizeof(atom), WRDLEN);
      obs->lastAtom->nextAtom = atomPtr;
      atomPtr->atomIdx = newIdx;
      atomPtr->prevAtom = obs->lastAtom;
      atomPtr->nextAtom = NULL;
      obs->lastAtom = atomPtr;
      obs->nAtoms++;
   }
}


/**
 *  \fn   void removeAtom(observation *obs, const int remIdx)
 *  \brief This function removes an atom with index remIdx from the  active set of observation obs
 *  \param obs: (inout) Observation structure where the atom is going to be removed
 *  \param remIdx: (input) Index to remove
*/
void removeAtom(observation *obs, const int remIdx)
{
   int i;
   atom *atomPtr=NULL;
	
   for(i=0, atomPtr=obs->firstAtom; i<obs->nAtoms; i++)
   {
      if (atomPtr->atomIdx == remIdx)
      {
         if (atomPtr->prevAtom != NULL)
            atomPtr->prevAtom->nextAtom = atomPtr->nextAtom;
         else
            obs->firstAtom = atomPtr->nextAtom;

         if (atomPtr->nextAtom != NULL)
            atomPtr->nextAtom->prevAtom = atomPtr->prevAtom;
         else
            obs->lastAtom = atomPtr->prevAtom; 

         obs->nAtoms--;
         nmf_free(atomPtr);
         break;
      }

      if (i!=obs->nAtoms - 1)  atomPtr=atomPtr->nextAtom;
   }
}


void freeObservations(int nObs, observation *obs)
{
   int i, j;
   atom *atomPtr=NULL;
	
   //atomPtr=obs[5].firstAtom;
   //nmf_free(atomPtr);
	
   for(i=0; i<nObs; i++)
   {
      for(j=1; j<obs[i].nAtoms; j++)
      {
         atomPtr=obs[i].lastAtom;

         if (atomPtr==NULL)
            printf("Atomptr is null\n");

         obs[i].lastAtom=atomPtr->prevAtom;

          nmf_free(atomPtr);
      }
      atomPtr=obs[i].lastAtom;

      nmf_free(atomPtr);
   }	
}


/**
 *  \fn    double derrorAsna(const int m, const int n, const int k, const double *A, const double *W, const double *H)
 *  \brief This function returns double precision error when error is computed using kulback liebler divergence
 *  \param m:    (input) Number of rows of A and W
 *  \param n:    (input) Number of columns of A and H
 *  \param k:    (input) Number of columns/rows of W/H  
 *  \param A:    (input) Double precision input matrix A
 *  \param W:    (input) Double precision input matrix W
 *  \param H:    (input) Double precision input matrix H
*/
double derrorAsna(const int m, const int n, const int k, const double *A, const double *W, const double *H)
{
  double error=0.0, *tmp=NULL;
	
  tmp=(double *)nmf_alloc(m * n * sizeof *tmp, WRDLEN);

  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, tmp, m);
  derrorbd1_cpu(m*n, A, tmp);
  
  error=cblas_dasum(m*n, tmp, 1);
    
  nmf_free(tmp);

  return error;
}


/**
 *  \fn    float serrorAsna(const int m, const int n, const int k, const float *A, const float *W, const float *H)
 *  \brief This function returns double precision error when error is computed using kulback liebler divergence
 *  \param m:    (input) Number of rows of A and W
 *  \param n:    (input) Number of columns of A and H
 *  \param k:    (input) Number of columns/rows of W/H  
 *  \param A:    (input) Simple precision input matrix A
 *  \param W:    (input) Simple precision input matrix W
 *  \param H:    (input) Simple precision input matrix H
*/
float serrorAsna(const int m, const int n, const int k, const float *A, const float *W, const float *H)
{
  float error=0.0, *tmp=NULL;
	
  tmp=(float *)nmf_alloc(m * n * sizeof *tmp, WRDLEN);

  cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, tmp, m);
  serrorbd1_cpu(m*n, A, tmp);
  
  error=cblas_sasum(m*n, tmp, 1);
    
  nmf_free(tmp);

  return error;
}


/**
 *  \fn   void dnrm1ColsSave(const int m, const int n, double *__restrict__ M, double *__restrict__ norms)
 *  \brief This function performs a column 1-norm normalization and returns the column norms
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Double precision input/output matrix (1D column-major)
 *  \param norms: (output) Double precision output vector of size n
*/
void dnrm1ColsSave(const int m, const int n, double *__restrict__ M, double *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
   {
      norms[i]=cblas_dasum(m, &M[i*m], 1) + dEPS;
      cblas_dscal(m, 1.0/norms[i], &M[i*m], 1);
   }
}


/**
 *  \fn   void snrm1ColsSave(const int m, const int n, float *__restrict__ M, float *__restrict__ norms)
 *  \brief This function performs a column 1-norm normalization and returns the column norms
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Simple precision input/output matrix (1D column-major)
 *  \param norms: (output) Simple precision output vector of size n
*/
void snrm1ColsSave(const int m, const int n, float *__restrict__ M, float *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
   {
      norms[i]=cblas_sasum(m, &M[i*m], 1) + sEPS;
      cblas_sscal(m, 1.0/norms[i], &M[i*m], 1);
   }
}


/**
 *  \fn   void dnrm2ColsSave(const int m, const int n, double *__restrict__ M, double *__restrict__ norms)
 *  \brief This function performs a column 2-norm normalization and returns the column norms
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Double precision input/output matrix (1D column-major)
 *  \param norms: (output) Double precision vector of size n
*/
void dnrm2ColsSave(const int m, const int n, double *__restrict__ M, double *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
   {
      norms[i]=cblas_dnrm2(m, &M[i*m], 1) + dEPS;
      cblas_dscal(m, 1.0/norms[i], &M[i*m], 1);
   }
}


/**
 *  \fn   void snrm2ColsSave(const int m, const int n, float *__restrict__ M, float *__restrict__ norms)
 *  \brief This function performs a column 2-norm normalization and returns the column norms
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Simple precision input/output matrix (1D column-major)
 *  \param norms: (output) Simple precision vector of size n
*/
void snrm2ColsSave(const int m, const int n, float *__restrict__ M, float *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
   {
      norms[i]=cblas_snrm2(m, &M[i*m], 1) + dEPS;
      cblas_sscal(m, 1.0/norms[i], &M[i*m], 1);
   }
}


/**
 *  \fn   void dcolsRescale(const int m, const int n, double *__restrict__ M, const double *__restrict__ norms)
 *  \brief This function performs a column scale back of matrix M using the original norms contained in norms 
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Double precision input/output matrix (1D column-major)
 *  \param norms: (input) Double precision vector of size n
*/
void dcolsRescale(const int m, const int n, double *__restrict__ M, const double *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
      cblas_dscal(m, norms[i], &M[i*m], 1);
}


/**
 *  \fn   void scolsRescale(const int m, const int n, float *__restrict__ M, const float *__restrict__ norms)
 *  \brief This function performs a column scale back of matrix M using the original norms contained in norms 
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Simple precision input/output matrix (1D column-major)
 *  \param norms: (input) Simple precision vector of size n
*/
void scolsRescale(const int m, const int n, float *__restrict__ M, const float *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
      cblas_sscal(m, norms[i], &M[i*m], 1);
}


/**
 *  \fn   void drowsRescale(const int m, const int n, double *__restrict__ M, const double *__restrict__ norms)
 *  \brief This function performs a row scale back of matrix M using the original norms contained in norms 
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Double precision input/output matrix (1D column-major)
 *  \param norms: (input) Double precision vector of size m
*/
void drowsRescale(const int m, const int n, double *__restrict__ M, const double *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<m; i++)
      cblas_dscal(n, norms[i], &M[i], m);
}


/**
 *  \fn   void srowsRescale(const int m, const int n, float *__restrict__ M, const float *__restrict__ norms)
 *  \brief This function performs a row scale back of matrix M using the original norms contained in norms 
 *  \param m: (input) Number of rows of M
 *  \param n: (input) Number of cols of M
 *  \param M: (inout) Simple precision input/output matrix (1D column-major)
 *  \param norms: (input) Simple precision vector of size m
*/
void srowsRescale(const int m, const int n, float *__restrict__ M, const float *__restrict__ norms)
{
   int i;
    
   #ifdef With_ICC
      #pragma loop_count min=32
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<m; i++)
      cblas_sscal(n, norms[i], &M[i], m);
}


/**
 *  \fn    void daddEsc_cpu(const int n, const double x, double *__restrict__ y)
 *  \brief This function performs double precision scalar-vector adition y[i]=x+y[i]
 *  \param n: (input) Number of elements of  y
 *  \param x: (input) Double precision scalar
 *  \param y: (inout) Double precision input/output vector/matrix (1D column-major)
*/
void daddEsc_cpu(const int n, const double x, double *__restrict__ y)
{
   int i;
   #ifdef With_ICC
      #pragma loop_count min=512
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
      y[i] = x + y[i];
}


/**
 *  \fn    void saddEsc_cpu(const int n, const float x, float *__restrict__ y)
 *  \brief This function performs double precision scalar-vector adition y[i]=x+y[i]
 *  \param n: (input) Number of elements of  y
 *  \param x: (input) Simple precision scalar
 *  \param y: (inout) Simple precision input/output vector/matrix (1D column-major)
*/
void saddEsc_cpu(const int n, const float x, float *__restrict__ y)
{
   int i;
   #ifdef With_ICC
      #pragma loop_count min=512
      #pragma omp simd
   #else
      #pragma omp parallel for
   #endif
   for(i=0; i<n; i++)
      y[i] = x + y[i];
}
