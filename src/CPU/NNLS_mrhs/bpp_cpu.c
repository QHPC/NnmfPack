/**
 *  \file    bpp_cpu.c
 *  \brief   File with functions to use the BPP algorithm to solve the NNLS problem with multiple rigth hand sides for CPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/

#include "bpp_cpu.h"

/* Updated Ranilla 27/2/2019 */
int dbpp_cpu(const int q, const int r, const int p, const double *C, const double *B, double *X)
{
   double *CtC=NULL, *CtB=NULL, *CtB_F=NULL, *CtB_G=NULL, *CtC_FF=NULL, *CtC_GF=NULL, *Y_G=NULL;
   int *F=NULL, *H1=NULL, *H2=NULL, *II=NULL, *sizeF=NULL, *sizeH1=NULL, *sizeH2=NULL;
   int *groups=NULL, *elems=NULL, *alpha=NULL, *beta=NULL;
   int i, j=0, k, ii, card, feasible, sizeI=0, noInfeasColumn, ptrI, ptrAux, i_F, i_G, setSizeG, nGroups=0;
   #ifndef With_MKL
     char Low='L';
   #endif

   elems =(int *)nmf_calloc(r, sizeof *elems, WRDLEN);

   F     =(int *)nmf_alloc(r * q * sizeof *F,      WRDLEN);
   H1    =(int *)nmf_alloc(r * q * sizeof *H1,     WRDLEN);
   H2    =(int *)nmf_alloc(r * q * sizeof *H2,     WRDLEN);
   II    =(int *)nmf_alloc(r     * sizeof *II,     WRDLEN);
   sizeF =(int *)nmf_alloc(r     * sizeof *sizeF,  WRDLEN);
   sizeH1=(int *)nmf_alloc(r     * sizeof *sizeH1, WRDLEN);
   sizeH2=(int *)nmf_alloc(r     * sizeof *sizeH2, WRDLEN);
   alpha =(int *)nmf_alloc(r     * sizeof *alpha,  WRDLEN);
   beta  =(int *)nmf_alloc(r     * sizeof *beta,   WRDLEN);
   groups=(int *)nmf_alloc(r * r * sizeof *groups, WRDLEN);

   CtC   =(double *)nmf_alloc(q * q * sizeof *CtC,    WRDLEN);
   CtB   =(double *)nmf_alloc(q * r * sizeof *CtB,    WRDLEN);
   CtB_F =(double *)nmf_alloc(q * r * sizeof *CtB_F,  WRDLEN);
   CtB_G =(double *)nmf_alloc(q * r * sizeof *CtB_G,  WRDLEN);
   CtC_FF=(double *)nmf_alloc(q * q * sizeof *CtC_FF, WRDLEN);
   CtC_GF=(double *)nmf_alloc(q * q * sizeof *CtC_GF, WRDLEN);
   Y_G   =(double *)nmf_alloc(q * r * sizeof *Y_G,    WRDLEN);
    
   if (elems==NULL  || F==NULL     || H1==NULL    || H2==NULL     || II==NULL     || sizeF==NULL) { return -1; }
   if (sizeH2==NULL || alpha==NULL || beta==NULL  || groups==NULL || CtC==NULL    || CtB==NULL)   { return -1; }
   if (sizeH1==NULL || CtB_F==NULL || CtB_G==NULL || CtC_FF==NULL || CtC_GF==NULL || Y_G==NULL)   { return -1; }

   // Compute C'C
   cblas_dsyrk(CblasColMajor, CblasLower, CblasTrans, q, p, 1, C, p, 0, CtC, q);

   //compute C'B
   cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, q, r, p, 1, C, p, B, p, 0, CtB, q);

   imemset_cpu(r, sizeF,  0);
   imemset_cpu(r, sizeH1, 0);
   imemset_cpu(r, sizeH2, 0);
	
   dmemset_cpu(q*r, X, 0);

   //init alpha = 3 and beta = q+1
   imemset_cpu(r, alpha, 3);
   imemset_cpu(r, beta,  q+1);
    
   //check if (X)Fj and (Y)Gj are feasible and find infeasible columns(I) 
   //Due to initialization Fj = {} and Gj has all indexes, so we check the condition for Y = -C'B for 1..q
   feasible = 1;
   #pragma omp parallel for schedule(dynamic) private(i, noInfeasColumn)
   for (j=0; j<r; j++)
   {
     noInfeasColumn = 1;
     for (i=0; i<q; i++)
     {
       if ((-CtB[i + j * q])  < 0) 
       {
         H2[sizeH2[j]++ + j * q] = i;
         if(noInfeasColumn)
         {
           feasible = 0;
           noInfeasColumn=0;
           #pragma omp critical
             II[sizeI++] = j;
         }
       }
     } // end for(i)
   } // end for(j)

   while(!feasible)
   {
     //Check if the number of feasibilities (card) is decreasing and update set F
     #pragma omp parallel for private(j, card)
     for (i=0; i<sizeI; i++)
     {
       j = II[i];
       card = sizeH1[j] + sizeH2[j];

       if(card < beta[j])
       {
         beta[j]  = card;
         alpha[j] = 3;
       }
       else
       {
         if(alpha[j] > 0)
           alpha[j]--;
         else 
         {
           if(sizeH1[j] == 0)
           {
             H2[0 + j*q] =  H2[(sizeH1[j] -1) + j*q];
             sizeH1[j] = 0; sizeH2[j] = 1;
           }
           else
           {
             if(sizeH2[j] == 0)
             {
               H1[0 + j*q] =  H1[(sizeH1[j] -1) + j*q];
               sizeH1[j] = 1; sizeH2[j] = 0;
             }
             else
             {
               if(H1[(sizeH1[j] -1) + j*q] > H2[(sizeH2[j] -1) + j*q])
               {
                 H1[0 + j*q] =  H1[(sizeH1[j] -1) + j*q];
                 sizeH1[j] = 1; sizeH2[j] = 0;
               }
               else
               {
                 H2[0 + j*q] =  H2[(sizeH1[j] -1) + j*q];
                 sizeH1[j] = 0; sizeH2[j] = 1;
               }
             }
           }
         }
       }     
                
       //Update F
       removeIdxs(&F[j*q], &sizeF[j], &H1[j*q], sizeH1[j]);
       addIdxs   (&F[j*q], &sizeF[j], &H2[j*q], sizeH2[j]);

       sizeH1[j]=0; sizeH2[j]=0;
     } // end for(i)

     //Column grouping
     if (r>1) //dont compute the column grouping for single rhs problem
     {
       nGroups = 0;
       imemset_cpu(r, elems, 0);
            
       while (sizeI > 0)
       {
         ptrI=0; ptrAux=0;
         j = II[ptrI++];
                
         //Create new group and add j to it
         groups[elems[nGroups] + nGroups * r] = j;
         elems[nGroups]++;
                
         while(ptrI < sizeI)
         {
           //add to the goup all indexes(k) wich F set is equal to j's SET
           k = II[ptrI++];
           if (setEquals(&F[j*q], sizeF[j], &F[k*q], sizeF[k]))
           {
             groups[elems[nGroups] + nGroups * r] = k;
             elems[nGroups]++;
           }
           else II[ptrAux++] = k;
         }
               
         sizeI = ptrAux;
         nGroups++;
       }
     }
     else
     {
       nGroups   = 1;
       groups[0] = j; // RANILLA BIG PROBLEM
       elems[0]  = 1;
     }
        
     //Compute  X(F(j)) and Y(G(j)) for all infeasible idx using column grouping and check feasibility 
     feasible = 1;
     sizeI    = 0;

     for(i=0; i<nGroups; i++)
     {
       j = groups[i*r];
       setSizeG = q - sizeF[j];

       dextractSubmatricesCtB(q, CtB, &F[j*q], sizeF[j], &groups[i*r], elems[i], CtB_F, CtB_G);
       dextractSubmatricesCtC(q, CtC, &F[j*q], sizeF[j], CtC_FF, CtC_GF);

       //Compute X(F(j))
       if(sizeF[j] != 0)
       {
         #ifdef With_MKL
           LAPACKE_dpotrf(LAPACK_COL_MAJOR, 'L', sizeF[j], CtC_FF, q);
           LAPACKE_dpotrs(LAPACK_COL_MAJOR, 'L', sizeF[j], elems[i], CtC_FF, q, CtB_F, q);
         #else
           k=q;
           dpotrf_(&Low, &sizeF[j], CtC_FF, &k, &ii);
           dpotrs_(&Low, &sizeF[j], &elems[i], CtC_FF, &k, CtB_F, &k, &ii);
         #endif
       }  

       //Compute Y(G(j))//TODO avoid computing YG(j) if (X)Fj is infeasible
       if(setSizeG != 0)
       {
         cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, setSizeG, elems[i], sizeF[j], 1, CtC_GF, q, CtB_F, q, 0, Y_G, q);
         dmatSub_cpu(setSizeG, elems[i], CtB_G, q, Y_G, q);
       }
           
       //check if (X)Fj and (Y)Gj are feasible and find infeasible columns(I)
       #pragma omp parallel for private(j,noInfeasColumn, ii, i_F,i_G)
       for(k=0; k<elems[i]; k++)
       {
         j = groups[k + i*r];
         noInfeasColumn = 1;
				
         for(ii=0, i_F=0, i_G=0; ii<q; ii++)
         {
           if (i_F < sizeF[j] && F[i_F + j *q] == ii) 
           {
             if (CtB_F[i_F + k * q]  < 0) 
             {
               H1[sizeH1[j]++ + j * q] = ii;
               if(noInfeasColumn)
               {
                 feasible = 0;
                 noInfeasColumn = 0;
                 #pragma omp critical
                   II[sizeI++] = j;
               }
               X[ii + j * q]  = 0;//TODO maybe this op is not necesary
             }
             else
               X[ii + j * q] = CtB_F[i_F + k * q]; //Copy X_F(j) to X
             i_F++;
           }
           else
           {
             if (Y_G[i_G+ k * q]  < 0) 
             {
               H2[sizeH2[j]++ + j * q] = ii;
               if(noInfeasColumn)
               {
                 feasible = 0;
                 noInfeasColumn = 0;
                 #pragma omp critical
                   II[sizeI++] = j;
               }
             }
             i_G++;
           }
         } // end for(ii)
       } // end for(k)
     } // enf for(i)
   } // while(!feasible)
        
   free(elems);
   nmf_free(F);
   nmf_free(II);
   nmf_free(H1);
   nmf_free(H2);
   nmf_free(CtC);
   nmf_free(CtB);
   nmf_free(Y_G);
   nmf_free(beta);
   nmf_free(alpha);
   nmf_free(sizeF);
   nmf_free(CtB_F);
   nmf_free(CtB_G);
   nmf_free(CtC_FF);
   nmf_free(CtC_GF);
   nmf_free(sizeH1);
   nmf_free(sizeH2);
   nmf_free(groups);

   return 0;
}


int sbpp_cpu(const int q, const int r, const int p, const float *C, const float *B, float *X)
{
   float *CtC=NULL, *CtB=NULL, *CtB_F=NULL, *CtB_G=NULL, *CtC_FF=NULL, *CtC_GF=NULL, *Y_G=NULL;
   int *F=NULL, *H1=NULL, *H2=NULL, *II=NULL, *sizeF=NULL, *sizeH1=NULL, *sizeH2=NULL;
   int *groups=NULL, *elems=NULL, *alpha=NULL, *beta=NULL;
   int i, j=0, k, ii, card, feasible, sizeI=0, noInfeasColumn, ptrI, ptrAux, i_F, i_G, setSizeG, nGroups=0;
   #ifndef With_MKL
     char Low='L';
   #endif

   elems =(int *)nmf_calloc(r, sizeof *elems, WRDLEN);

   F     =(int *)nmf_alloc(r * q * sizeof *F,      WRDLEN);
   H1    =(int *)nmf_alloc(r * q * sizeof *H1,     WRDLEN);
   H2    =(int *)nmf_alloc(r * q * sizeof *H2,     WRDLEN);
   II    =(int *)nmf_alloc(r     * sizeof *II,     WRDLEN);
   sizeF =(int *)nmf_alloc(r     * sizeof *sizeF,  WRDLEN);
   sizeH1=(int *)nmf_alloc(r     * sizeof *sizeH1, WRDLEN);
   sizeH2=(int *)nmf_alloc(r     * sizeof *sizeH2, WRDLEN);
   alpha =(int *)nmf_alloc(r     * sizeof *alpha,  WRDLEN);
   beta  =(int *)nmf_alloc(r     * sizeof *beta,   WRDLEN);
   groups=(int *)nmf_alloc(r * r * sizeof *groups, WRDLEN);

   CtC   =(float *)nmf_alloc(q * q * sizeof *CtC,    WRDLEN);
   CtB   =(float *)nmf_alloc(q * r * sizeof *CtB,    WRDLEN);
   CtB_F =(float *)nmf_alloc(q * r * sizeof *CtB_F,  WRDLEN);
   CtB_G =(float *)nmf_alloc(q * r * sizeof *CtB_G,  WRDLEN);
   CtC_FF=(float *)nmf_alloc(q * q * sizeof *CtC_FF, WRDLEN);
   CtC_GF=(float *)nmf_alloc(q * q * sizeof *CtC_GF, WRDLEN);
   Y_G   =(float *)nmf_alloc(q * r * sizeof *Y_G,    WRDLEN);
    
   if (elems==NULL  || F==NULL     || H1==NULL    || H2==NULL     || II==NULL     || sizeF==NULL) { return -1; }
   if (sizeH2==NULL || alpha==NULL || beta==NULL  || groups==NULL || CtC==NULL    || CtB==NULL)   { return -1; }
   if (sizeH1==NULL || CtB_F==NULL || CtB_G==NULL || CtC_FF==NULL || CtC_GF==NULL || Y_G==NULL)   { return -1; }

   // Compute C'C
   cblas_ssyrk(CblasColMajor, CblasLower, CblasTrans, q, p, 1, C, p, 0, CtC, q);

   //compute C'B
   cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, q, r, p, 1, C, p, B, p, 0, CtB, q);

   imemset_cpu(r, sizeF,  0);
   imemset_cpu(r, sizeH1, 0);
   imemset_cpu(r, sizeH2, 0);
	
   smemset_cpu(q*r, X, 0);

   //init alpha = 3 and beta = q+1
   imemset_cpu(r, alpha, 3);
   imemset_cpu(r, beta,  q+1);
    
   //check if (X)Fj and (Y)Gj are feasible and find infeasible columns(I) 
   //Due to initialization Fj = {} and Gj has all indexes, so we check the condition for Y = -C'B for 1..q
   feasible = 1;
   #pragma omp parallel for schedule(dynamic) private(i, noInfeasColumn)
   for (j=0; j<r; j++)
   {
     noInfeasColumn = 1;
     for (i=0; i<q; i++)
     {
       if ((-CtB[i + j * q])  < 0) 
       {
         H2[sizeH2[j]++ + j * q] = i;
         if(noInfeasColumn)
         {
           feasible = 0;
           noInfeasColumn=0;
           #pragma omp critical
             II[sizeI++] = j;
         }
       }
     } // end for(i)
   } // end for(j)

   while(!feasible)
   {
     //Check if the number of feasibilities (card) is decreasing and update set F
     #pragma omp parallel for private(j, card)
     for (i=0; i<sizeI; i++)
     {
       j = II[i];
       card = sizeH1[j] + sizeH2[j];

       if(card < beta[j])
       {
         beta[j]  = card;
         alpha[j] = 3;
       }
       else
       {
         if(alpha[j] > 0)
           alpha[j]--;
         else 
         {
           if(sizeH1[j] == 0)
           {
             H2[0 + j*q] =  H2[(sizeH1[j] -1) + j*q];
             sizeH1[j] = 0; sizeH2[j] = 1;
           }
           else
           {
             if(sizeH2[j] == 0)
             {
               H1[0 + j*q] =  H1[(sizeH1[j] -1) + j*q];
               sizeH1[j] = 1; sizeH2[j] = 0;
             }
             else
             {
               if(H1[(sizeH1[j] -1) + j*q] > H2[(sizeH2[j] -1) + j*q])
               {
                 H1[0 + j*q] =  H1[(sizeH1[j] -1) + j*q];
                 sizeH1[j] = 1; sizeH2[j] = 0;
               }
               else
               {
                 H2[0 + j*q] =  H2[(sizeH1[j] -1) + j*q];
                 sizeH1[j] = 0; sizeH2[j] = 1;
               }
             }
           }
         }
       }     
                
       //Update F
       removeIdxs(&F[j*q], &sizeF[j], &H1[j*q], sizeH1[j]);
       addIdxs   (&F[j*q], &sizeF[j], &H2[j*q], sizeH2[j]);

       sizeH1[j]=0; sizeH2[j]=0;
     } // end for(i)

     //Column grouping
     if (r>1) //dont compute the column grouping for single rhs problem
     {
       nGroups = 0;
       imemset_cpu(r, elems, 0);
            
       while (sizeI > 0)
       {
         ptrI=0; ptrAux=0;
         j = II[ptrI++];
                
         //Create new group and add j to it
         groups[elems[nGroups] + nGroups * r] = j;
         elems[nGroups]++;
                
         while(ptrI < sizeI)
         {
           //add to the goup all indexes(k) wich F set is equal to j's SET
           k = II[ptrI++];
           if (setEquals(&F[j*q], sizeF[j], &F[k*q], sizeF[k]))
           {
             groups[elems[nGroups] + nGroups * r] = k;
             elems[nGroups]++;
           }
           else II[ptrAux++] = k;
         }
               
         sizeI = ptrAux;
         nGroups++;
       }
     }
     else
     {
       nGroups   = 1;
       groups[0] = j; // RANILLA BIG PROBLEM
       elems[0]  = 1;
     }
        
     //Compute  X(F(j)) and Y(G(j)) for all infeasible idx using column grouping and check feasibility 
     feasible = 1;
     sizeI    = 0;

     for(i=0; i<nGroups; i++)
     {
       j = groups[i*r];
       setSizeG = q - sizeF[j];

       sextractSubmatricesCtB(q, CtB, &F[j*q], sizeF[j], &groups[i*r], elems[i], CtB_F, CtB_G);
       sextractSubmatricesCtC(q, CtC, &F[j*q], sizeF[j], CtC_FF, CtC_GF);

       //Compute X(F(j))
       if(sizeF[j] != 0)
       {
         #ifdef With_MKL
           LAPACKE_spotrf(LAPACK_COL_MAJOR, 'L', sizeF[j], CtC_FF, q);
           LAPACKE_spotrs(LAPACK_COL_MAJOR, 'L', sizeF[j], elems[i], CtC_FF, q, CtB_F, q);
         #else
           k=q;
           spotrf_(&Low, &sizeF[j], CtC_FF, &k, &ii);
           spotrs_(&Low, &sizeF[j], &elems[i], CtC_FF, &k, CtB_F, &k, &ii);
         #endif
       }  

       //Compute Y(G(j))//TODO avoid computing YG(j) if (X)Fj is infeasible
       if(setSizeG != 0)
       {
         cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, setSizeG, elems[i], sizeF[j], 1, CtC_GF, q, CtB_F, q, 0, Y_G, q);
         smatSub_cpu(setSizeG, elems[i], CtB_G, q, Y_G, q);
       }
           
       //check if (X)Fj and (Y)Gj are feasible and find infeasible columns(I)
       #pragma omp parallel for private(j,noInfeasColumn, ii, i_F,i_G)
       for(k=0; k<elems[i]; k++)
       {
         j = groups[k + i*r];
         noInfeasColumn = 1;
				
         for(ii=0, i_F=0, i_G=0; ii<q; ii++)
         {
           if (i_F < sizeF[j] && F[i_F + j *q] == ii) 
           {
             if (CtB_F[i_F + k * q]  < 0) 
             {
               H1[sizeH1[j]++ + j * q] = ii;
               if(noInfeasColumn)
               {
                 feasible = 0;
                 noInfeasColumn = 0;
                 #pragma omp critical
                   II[sizeI++] = j;
               }
               X[ii + j * q]  = 0;//TODO maybe this op is not necesary
             }
             else
               X[ii + j * q] = CtB_F[i_F + k * q]; //Copy X_F(j) to X
             i_F++;
           }
           else
           {
             if (Y_G[i_G+ k * q]  < 0) 
             {
               H2[sizeH2[j]++ + j * q] = ii;
               if(noInfeasColumn)
               {
                 feasible = 0;
                 noInfeasColumn = 0;
                 #pragma omp critical
                   II[sizeI++] = j;
               }
             }
             i_G++;
           }
         } // end for(ii)
       } // end for(k)
     } // enf for(i)
   } // while(!feasible)
        
   free(elems);
   nmf_free(F);
   nmf_free(II);
   nmf_free(H1);
   nmf_free(H2);
   nmf_free(CtC);
   nmf_free(CtB);
   nmf_free(Y_G);
   nmf_free(beta);
   nmf_free(alpha);
   nmf_free(sizeF);
   nmf_free(CtB_F);
   nmf_free(CtB_G);
   nmf_free(CtC_FF);
   nmf_free(CtC_GF);
   nmf_free(sizeH1);
   nmf_free(sizeH2);
   nmf_free(groups);

   return 0;
}


void removeIdxs(int *setA, int *sizeA, const int *setB, const int sizeB)
{
   int ptrRead , ptrWrite, ptrB, outSize=0;
    
   //don't do anithing if B ={}
   if(sizeB > 0)
   {
      for( ptrRead=ptrWrite=ptrB=0; ptrRead<(*sizeA); ptrRead++)
      {
         // Move ptrB
         if ((setA[ptrRead]>setB[ptrB]) && (ptrB<sizeB -1)) { ptrB++; }
            
         //Copy everithing from setA to setA except the idxs in setB
         if (setA[ptrRead] != setB[ptrB])
         {   
            setA[ptrWrite++] = setA[ptrRead];
            outSize++;
         }
      }
      *sizeA = outSize;
   }
}


void addIdxs(int *setA, int *sizeA, const int *setB, const int sizeB)
{
   int ptrA=0, ptrB=0, ptrR=0, notFinished=1;
   int *setR=NULL;
    
   setR =(int *)nmf_alloc((*sizeA + sizeB)* sizeof *setR, WRDLEN);
        
   if (*sizeA == 0)
   {
      memcpy(setA, setB, sizeof(int)*sizeB);
      *sizeA = sizeB;
   }
   else
   {
      if (sizeB != 0)
      {
         while(notFinished)
         {
            if (setA[ptrA] == setB[ptrB])
            {
               setR[ptrR++] = setA[ptrA++];
               ptrB++;
            }
            else
            {
               if (setA[ptrA] < setB[ptrB])
                  setR[ptrR++] = setA[ptrA++];
               else
                  setR[ptrR++] = setB[ptrB++];
            }    

            //If we reach the end of one of the two sets, copy the rest of the other set.
            if (ptrA == *sizeA)
            {
                while (ptrB < sizeB)
                   setR[ptrR++] = setB[ptrB++];
                notFinished = 0;
            }
            else
            {
               if (ptrB == sizeB)
               {
                  while(ptrA < *sizeA)
                     setR[ptrR++] = setA[ptrA++];
                  notFinished = 0;
               }
            }    
         }
         *sizeA = ptrR;
            
         //overwrite setA with the resultSet
         for(ptrA=0; ptrA<(*sizeA); ptrA++) { setA[ptrA] = setR[ptrA]; }
      }
   }
   nmf_free(setR);
}


//TODO paralelizar y optimizar
int setEquals(const int *setA, const int sizeA, const int* setB, const int sizeB)
{
   int i, equals=1;
    
   if(sizeA != sizeB)
      equals = 0;
   else
      for (i=0; i<sizeA && equals; i++)
         if(setA[i] != setB[i]) { equals = 0; }

   return equals;
}


void dextractSubmatricesCtC(const int q ,const double *CtC, int *F, const int setSize, double *CtC_FF, double *CtC_GF)
{
   int i, j, i_F, i_G, idx;

   #pragma omp parallel for private(i, i_F, i_G, idx)
   for(j=0; j<setSize; j++)
   {
      idx = F[j]; 
        
      for(i=0, i_F=0, i_G=0; i<idx; i++)
         if (F[i_F] == i) 
         {
            //CtC_FF[i_F + j * q ] = CtC[ i + idx * q];
            i_F++;
         }
         else
         {
            CtC_GF[i_G + j * q ] = CtC[idx + i * q];//TODO revisar indexacion
            i_G++;
         }

      for(i=idx; i<q; i++)
         if (i_F<setSize && F[i_F]==i) //if the index is in F copy from CtC to CtC_FF
         {
            CtC_FF[i_F + j * q ] = CtC[ i + idx * q];
            i_F++;
         }
         else//if the index is in G (not in F) copy from CtC to CtC_GF
         {
            CtC_GF[i_G + j * q ] = CtC[ i + idx * q];
            i_G++;
         }
   }
}


void sextractSubmatricesCtC(const int q ,const float *CtC, int *F, const int setSize, float *CtC_FF, float *CtC_GF)
{
   int i, j, i_F, i_G, idx;

   #pragma omp parallel for private(i, i_F, i_G, idx)
   for(j=0; j<setSize; j++)
   {
      idx = F[j]; 
        
      for(i=0, i_F=0, i_G=0; i<idx; i++)
         if (F[i_F] == i) 
         {
            //CtC_FF[i_F + j * q ] = CtC[ i + idx * q];
            i_F++;
         }
         else
         {
            CtC_GF[i_G + j * q ] = CtC[idx + i * q];//TODO revisar indexacion
            i_G++;
         }

      for(i=idx; i<q; i++)
         if (i_F<setSize && F[i_F]==i) //if the index is in F copy from CtC to CtC_FF
         {
            CtC_FF[i_F + j * q ] = CtC[ i + idx * q];
            i_F++;
         }
         else//if the index is in G (not in F) copy from CtC to CtC_GF
         {
            CtC_GF[i_G + j * q ] = CtC[ i + idx * q];
            i_G++;
         }
   }
}


void dextractSubmatricesCtB(const int q ,const double* CtB, int* F, const int setSize, const int* group, const int nElems, double* CtB_F, double* CtB_G)
{
   int i, j, i_F, i_G, idx;

   #pragma omp parallel for private(i,i_F,i_G,idx)
   for(j=0; j<nElems; j++)
   {
      idx = group[j];
      for(i=0, i_F=0, i_G=0; i<q; i++)
         //if the index is in F copy from CtB to CtB_F
         if (i_F < setSize && F[i_F] == i) 
         {
            CtB_F[i_F + j * q ] = CtB[ i + idx * q];
            i_F++;
         }
         else//if the index is in G (not in F) copy from CtB to CtB_G
         {
             CtB_G[i_G + j * q ] = CtB[ i + idx * q];
             i_G++;
         }
   }
}


void sextractSubmatricesCtB(const int q ,const float* CtB, int* F, const int setSize, const int* group, const int nElems, float* CtB_F, float* CtB_G)
{
   int i, j, i_F, i_G, idx;

   #pragma omp parallel for private(i,i_F,i_G,idx)
   for(j=0; j<nElems; j++)
   {
      idx = group[j];
      for(i=0, i_F=0, i_G=0; i<q; i++)
         //if the index is in F copy from CtB to CtB_F
         if (i_F < setSize && F[i_F] == i) 
         {
            CtB_F[i_F + j * q ] = CtB[ i + idx * q];
            i_F++;
         }
         else//if the index is in G (not in F) copy from CtB to CtB_G
         {
             CtB_G[i_G + j * q ] = CtB[ i + idx * q];
             i_G++;
         }
   }
}
