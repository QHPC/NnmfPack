/**
 *  \file    nnls_cpu.h
 *  \brief   Interface file for the NNLS and NNLS-like algorithms algorithms
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef NNLS_CPU_H
#define NNLS_CPU_H

int dbpp_cpu(const int, const int, const int, const double *, const double *, double *);
int sbpp_cpu(const int, const int, const int, const float  *, const float *,  float *);

int dasna_cpu(const int, const int, const int, const double *, const double *, double *, const int, const int);
int sasna_cpu(const int, const int, const int, const float *,  const float *,  float *,  const int, const int);

int dpbdiv_cpu(const int, const int, const int, const double *, double *, double *, const double, const int, const int);
int spbdiv_cpu(const int, const int, const int, const float *,  float *,  float *,  const float,  const int, const int);

int dpbdivRestrict_cpu(const int, const int, const int,    const double *, double *, double *, const double,
                       const int, const int, const double, const double,   const unsigned short); 
int spbdivRestrict_cpu(const int, const int, const int,    const float *,  float *,  float *,  const float,
                       const int, const int, const float,  const float,    const unsigned short);
#endif
