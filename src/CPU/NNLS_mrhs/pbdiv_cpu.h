/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    pbdiv_cpu.h
 *  \brief   Header file for using the partial betadivergence functions with CPU
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#ifndef PBDIV_CPU_H
#define PBDIV_CPU_H

#include "bdiv_cpu.h"
#include "nnls_cpu.h"// Public interface of the library

#define UpdateAll 1
#define UpdateW   2
#define UpdateH   3

//TODO change to partials

/* inner functions */
/* general cases   */
int dpbdivg_cpu(const int, const int, const int, const double *, double *, double *, const double, const int, const int);
int spbdivg_cpu(const int, const int, const int, const float  *, float  *, float  *, const float,  const int, const int);

int dpbdivgRestrict_cpu(const int, const int, const int,    const double *, double *, double *, const double,
                        const int, const int, const double, const double, const unsigned short); 
int spbdivgRestrict_cpu(const int, const int, const int,    const float *,  float *,  float *,  const float,
                        const int, const int, const float,  const float,  const unsigned short);

/* beta=2 */
int dpmlsaRestrict_cpu(const int, const int, const int,    const double *, double *, double *, 
                       const int, const int, const double, const double,   const unsigned short); 
int spmlsaRestrict_cpu(const int, const int, const int,    const float *,  float *,  float *,
                       const int, const int, const float,  const float,    const unsigned short);

/* beta=1 */
int dpbdivone_cpu(const int, const int, const int, const double *, double *, double *, const int, const int);
int spbdivone_cpu(const int, const int, const int, const float  *, float  *, float  *, const int, const int);

int dpbdivoneRestrict_cpu(const int, const int, const int,    const double *, double *, double *,
                          const int, const int, const double, const double,   const unsigned short); 
int spbdivoneRestrict_cpu(const int, const int, const int,    const float *,  float *,  float *,
                          const int, const int, const float,  const float,    const unsigned short);

#endif
