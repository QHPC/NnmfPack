/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    utils_cpu.h
 *  \brief   Header file for using utility modules from CPU/MIC source codes.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/011/14
*/
#ifndef UTILSCPU_H
#define UTILSCPU_H

#define _Float128 __float128 //temporary fix to avoid problem with icc < 18.01 and glibc 2.6
#define dEPS 1e-16           //small value to avoid zeros double precision
#define sEPS 1e-08           //small value to avoid zeros simple precision
#define WRDLEN 16            //ICC recomienda 32 para CPUs y 64 para los MIC primera genacion

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#ifdef With_MKL
  #include <mkl.h>
#else
  #include <cblas.h>
  extern void dlarnv_(int *, int *, int *, double *);
  extern void slarnv_(int *, int *, int *, float  *);

  extern void dpotrf_(char *, int *, double *, int *, int *);
  extern void spotrf_(char *, int *, float *,  int *, int *);

  extern void dpotrs_(char *, int *, int *, double *, int *, double *, int *, int *);
  extern void spotrs_(char *, int *, int *, float *,  int *, float *,  int *, int *);
#endif

#ifdef With_OMP
  #include <omp.h>
#endif

#ifdef OMP4
  #include <float.h>

  struct dPosMin {
    double val;
    int    pos;
  };
  typedef struct dPosMin dPosMin;

  struct sPosMin {
    float val;
    int   pos;
  };
  typedef struct sPosMin sPosMin;

  #pragma omp declare reduction(dMIN : dPosMin : \
    omp_out = ((omp_in.val < omp_out.val) || ((omp_in.val == omp_out.val) && (omp_in.pos < omp_out.pos))) ? omp_in : omp_out) \
    initializer(omp_priv = { DBL_MAX, 0 })

  #pragma omp declare reduction(sMIN : sPosMin : \
    omp_out = ((omp_in.val < omp_out.val) || ((omp_in.val == omp_out.val) && (omp_in.pos < omp_out.pos))) ? omp_in : omp_out) \
    initializer(omp_priv = { FLT_MAX, 0 })
#endif

#define max(a,b) (((a)>(b ))?( a):(b))
#define min(a,b) (((a)<(b ))?( a):(b))

#ifdef With_MKL
  #define nmf_alloc(size, align) mkl_malloc((size),(align))
  #define nmf_free(ptr) mkl_free(ptr)
  #define nmf_calloc(num, size_t, align) mkl_calloc((num),(size_t),(align))
#else
  #define nmf_calloc(num, size_t, align) calloc((num),(size_t))
  #if (AllocModel == 1) // mm_malloc
    #include <mm_malloc.h>
    #define nmf_alloc(size, align) _mm_malloc((size),(align))
    #define nmf_free(ptr) _mm_free(ptr)
  #else
    #if (AllocModel == 2) // aligned_alloc, -std=c11 ?
      #define nmf_alloc(size, align) aligned_alloc((size),(size))
    #else
      #if (AllocModel == 3) // posix_memaling
        static inline void *pos_alloc(int size, int align)
        {
          void *ptr=NULL;
          posix_memalign(&ptr, align, size);
          return ptr;
        }
        #define nmf_alloc(size, align) pos_alloc((size),(align))
      #else
        #define nmf_alloc(size,align) malloc((size))
      #endif
    #endif
    #define nmf_free(ptr) free((ptr))
  #endif
#endif

#ifdef With_MKL
  #define NMFFUNC(FUNC) FUNC
  #define nmf_daxpy(N, SA, SX, INCX, SY, INCY) NMFFUNC(cblas_daxpy)((N),(SA),(SX),(INCX),(SY),(INCY))
  #define nmf_saxpy(N, SA, SX, INCX, SY, INCY) NMFFUNC(cblas_saxpy)((N),(SA),(SX),(INCX),(SY),(INCY))
#else
  #if (AllocBlas == 1) // interface cblas, like MKL
    #define NMFFUNC(FUNC) FUNC
    #define nmf_daxpy(N, SA, SX, INCX, SY, INCY) NMFFUNC(cblas_daxpy)((N),(SA),(SX),(INCX),(SY),(INCY))
    #define nmf_saxpy(N, SA, SX, INCX, SY, INCY) NMFFUNC(cblas_saxpy)((N),(SA),(SX),(INCX),(SY),(INCY))
  #else
    #if (AllocBlas == 2) // interface fortran, the classical one
      #define NMFFUNC(FUNC) FUNC##_
      #define nmf_daxpy(N, SA, SX, INCX, SY, INCY) NMFFUNC(daxpy)((N),(SA),(SX),(INCX),(SY),(INCY))
      #define nmf_saxpy(N, SA, SX, INCX, SY, INCY) NMFFUNC(saxpy)((N),(SA),(SX),(INCX),(SY),(INCY))
    #endif
  #endif
#endif


void dmemset_cpu(const int n, double *__restrict__, const double);
void smemset_cpu(const int n, float  *__restrict__, const float);

void imemset_cpu(const int n, int    *__restrict__, const int);

void ddiv_cpu(const int, const double *, const double *, double *__restrict__);
void sdiv_cpu(const int, const float  *, const float *,  float  *__restrict__);

void ddotdiv_cpu(const int, const double *, const double *, double *__restrict__);
void sdotdiv_cpu(const int, const float  *, const float  *, float  *__restrict__);

void dmult_cpu(const int, const double *, const double *,  double *__restrict__);
void smult_cpu(const int, const float  *, const float  *,  float  *__restrict__);

void dsub_cpu(const int, const double *, const double *, double *__restrict__);
void ssub_cpu(const int, const float  *, const float *,  float  *__restrict__);

void dmatSub_cpu(const int, const int, const double *, const int, double *__restrict__, const int);
void smatSub_cpu(const int, const int, const float  *, const int, float  *__restrict__, const int);

void dlarngenn_cpu(const int, const int, const int, double *);
void slarngenn_cpu(const int, const int, const int, float  *);

double derror_cpu(const int, const int, const int, const double *, const double *, const double *);
float  serror_cpu(const int, const int, const int, const float  *, const float  *, const float  *);

void dnrm2Cols_cpu(const int, const int, double *__restrict__);
void snrm2Cols_cpu(const int, const int, float  *__restrict__);

void dtrans_cpu(const int, const int, const double *, double *__restrict__);
void strans_cpu(const int, const int, const float  *, float  *__restrict__);

void dhalfwave_cpu(const int, double *__restrict__, const int);
void shalfwave_cpu(const int, float  *__restrict__, const int);

int Idmin_cpu (const int, const double *__restrict__);
int Ismin_cpu (const int, const float  *__restrict__);

int Idamin_cpu(const int, const double *__restrict__);
int Isamin_cpu(const int, const float  *__restrict__);

#endif
