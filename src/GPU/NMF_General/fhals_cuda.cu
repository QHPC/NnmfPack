/**
 *  \file    fhals_cuda.h
 *  \brief   File with functions to calcule NNMF using the fast HALS algorithm for GPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    09/05/18
*/

#include "fhals_cuda.h"


/** TODO documentar
 * 
 */
int dfhals_cuda(const int m, const int n, const int k, const double *A, double *W, double *H, const int nIter)
{
  double *B=NULL, *Mat1=NULL, *Mat2=NULL, norm, one=1.0, minusOne=-1.0, zero=0.0;
  int     i, j;

  cublasHandle_t handle1, handle2;
  cudaStream_t   stream1, stream2;
  cudaEvent_t    event1,  event2;
    
  CUDAERR(cudaStreamCreate(&stream1));
  CUDAERR(cudaStreamCreate(&stream2));

  CUBLASERR(cublasCreate(&handle1));
  CUBLASERR(cublasCreate(&handle2));

  CUDAERR(cudaEventCreateWithFlags(&event1, cudaEventDisableTiming));
  CUDAERR(cudaEventCreateWithFlags(&event2, cudaEventDisableTiming)); 

  CUBLASERR(cublasSetStream(handle1, stream1));
  CUBLASERR(cublasSetStream(handle2, stream2));
	
  CUDAERR(cudaMalloc((void **)&B, n*k*sizeof(double)));
  CUDAERR(cudaMalloc((void **)&Mat1, max(m,n)*k*sizeof(double)));
  CUDAERR(cudaMalloc((void **)&Mat2, k*k*sizeof(double)));

  //dtrans(k, n, H, B);
  CUBLASERR(cublasDgeam(handle1, CUBLAS_OP_T, CUBLAS_OP_N, n, k, &one, H, k, &zero,B, n, B, n));
     
  //normalization to unit l2-norm length
  dnrm2Cols_cuda(handle1, m, k, W);
      
  //initialy both  streams can execute
  CUDAERR(cudaEventRecord(event2, stream1));
	  
  for (i=0; i<nIter; i++)
  {
     // Stream2 waiting for event2 to continue executing 
     CUDAERR(cudaStreamWaitEvent(stream2, event2, 0));
		   
     //Mat1 = A'* W;
     CUBLASERR(cublasDgemm(handle2, CUBLAS_OP_T, CUBLAS_OP_N, n, k, m, &one, A, m, W, m, &zero, Mat1, n));

     //Stream 2 signals stream 1 that has finished its part through event1
     CUDAERR(cudaEventRecord(event1, stream2));
		 
     //MAt2 = W' * W;
     CUBLASERR(cublasDgemm(handle1, CUBLAS_OP_T, CUBLAS_OP_N, k, k, m, &one, W, m, W, m, &zero, Mat2, k));
        
     //Stream 1 waits for stream 2 to finish its part
     CUDAERR(cudaStreamWaitEvent(stream1, event1, 0));

     for (j=0; j<k; j++)
     {
        //B(:,j) = max( B(:,j) + Mat1(:,j) - B * Mat2(:,j), dEPS)
        //B(:,j) = max( B(:,j) + - B * Mat2(:,j) + Mat1(:,j), dEPS)
        CUBLASERR(cublasDgemv(handle1, CUBLAS_OP_N, n, k, &minusOne, B, n,  &Mat2[j*k], 1, &one, &Mat1[j*n], 1));
        CUBLASERR(cublasDaxpy(handle1,n, &one, &Mat1[j*n], 1, &B[j*n], 1));
        dhalfwave_cuda(n, &B[j*n]);
     }

     //Stream 1 signals stream 2 so it can start working
     CUDAERR(cudaEventRecord(event2, stream1));
        
     //Stream2 waiting for event2 to continue executing 
     CUDAERR(cudaStreamWaitEvent(stream2, event2, 0));

     //Mat3 = A * B
     CUBLASERR(cublasDgemm(handle2, CUBLAS_OP_N, CUBLAS_OP_N, m, k, n, &one, A, m, B, n, &zero, Mat1, m));

     //Stream 2 signals stream 1 that has finished its part through event1
     CUDAERR(cudaEventRecord(event1, stream2));
		
     //Mat2 = B' * B
     CUBLASERR(cublasDgemm(handle1, CUBLAS_OP_T, CUBLAS_OP_N, k, k, n, &one, B, n, B, n, &zero, Mat2, k));
        
     //Stream 1 waits for stream 2 to finish its part
     CUDAERR(cudaStreamWaitEvent(stream1, event1, 0));

     for (j=0; j<k; j++)
     {
        //W(:,j) = max( W(:,j) * Mat2(j,j) + Mat3(:,j) - W * Mat2(:,j) , 0)
        CUBLASERR(cublasDgemv(handle1, CUBLAS_OP_N, m, k, &minusOne, W, m,  &Mat2[j*k], 1, &one, &Mat1[j*m], 1));

        //pointer mode change needed because Mat2(j,j) is in device
        CUBLASERR(cublasSetPointerMode(handle1, CUBLAS_POINTER_MODE_DEVICE));        
        CUBLASERR(cublasDaxpy(handle1, m, &Mat2[j+j*k], &W[j*m], 1, &Mat1[j*m], 1));

        CUBLASERR(cublasSetPointerMode(handle1, CUBLAS_POINTER_MODE_HOST));
        CUBLASERR(cublasDcopy(handle1, m, &Mat1[j*m], 1, &W[j*m], 1));
        dhalfwave_cuda(m, &W[j*m]);
            
        //W(:,j) = W(:,j) / norm(W(:,j));
        CUBLASERR(cublasDnrm2(handle1, m, &W[j*m], 1, &norm));
        norm = 1.0/norm;
        CUBLASERR(cublasDscal(handle1, m, &norm, &W[j*m], 1));
     }
     //Stream 1 signals stream 2 so it can start working
     CUDAERR(cudaEventRecord(event2, stream1));
  }
    
  //dtrans(n, k, B, H);
  CUBLASERR(cublasDgeam(handle1, CUBLAS_OP_T, CUBLAS_OP_N, k, n, &one, B, n, &zero,H, k, H, k));
    
  CUBLASERR(cublasDestroy(handle1));
  CUBLASERR(cublasDestroy(handle2));

  CUDAERR(cudaStreamDestroy(stream1));
  CUDAERR(cudaStreamDestroy(stream2)); 

  CUDAERR(cudaEventDestroy(event1));
  CUDAERR(cudaEventDestroy(event2)); 
  
  CUDAERR(cudaFree(B));
  CUDAERR(cudaFree(Mat1));
  CUDAERR(cudaFree(Mat2));
    
  return 0;
}


/** TODO documentar
 * 
 */
int sfhals_cuda(const int m, const int n, const int k, const float *A, float *W, float *H, const int nIter)
{
  float *B=NULL, *Mat1=NULL, *Mat2=NULL, norm, one=1.0f, minusOne=-1.0f, zero=0.0f;
  int    i, j;

  cublasHandle_t handle1, handle2;
  cudaStream_t   stream1, stream2;
  cudaEvent_t    event1,  event2;
    
  CUDAERR(cudaStreamCreate(&stream1));
  CUDAERR(cudaStreamCreate(&stream2));

  CUBLASERR(cublasCreate(&handle1));
  CUBLASERR(cublasCreate(&handle2));

  CUDAERR(cudaEventCreateWithFlags(&event1, cudaEventDisableTiming));
  CUDAERR(cudaEventCreateWithFlags(&event2, cudaEventDisableTiming)); 

  CUBLASERR(cublasSetStream(handle1, stream1));
  CUBLASERR(cublasSetStream(handle2, stream2));
	
  CUDAERR(cudaMalloc((void **)&B, n*k*sizeof(float)));
  CUDAERR(cudaMalloc((void **)&Mat1, max(m,n)*k*sizeof(float)));
  CUDAERR(cudaMalloc((void **)&Mat2, k*k*sizeof(float)));

  //strans(k, n, H, B);
  CUBLASERR(cublasSgeam(handle1, CUBLAS_OP_T, CUBLAS_OP_N, n, k, &one, H, k, &zero,B, n, B, n));
     
  //normalization to unit l2-norm length
  snrm2Cols_cuda(handle1, m, k, W);
      
  //initialy both  streams can execute
  CUDAERR(cudaEventRecord(event2, stream1));
	  
  for (i=0; i<nIter; i++)
  {
     // Stream2 waiting for event2 to continue executing 
     CUDAERR(cudaStreamWaitEvent(stream2, event2, 0));
		   
     //Mat1 = A'* W;
     CUBLASERR(cublasSgemm(handle2, CUBLAS_OP_T, CUBLAS_OP_N, n, k, m, &one, A, m, W, m, &zero, Mat1, n));

     //Stream 2 signals stream 1 that has finished its part through event1
     CUDAERR(cudaEventRecord(event1, stream2));
		 
     //MAt2 = W' * W;
     CUBLASERR(cublasSgemm(handle1, CUBLAS_OP_T, CUBLAS_OP_N, k, k, m, &one, W, m, W, m, &zero, Mat2, k));
        
     //Stream 1 waits for stream 2 to finish its part
     CUDAERR(cudaStreamWaitEvent(stream1, event1, 0));

     for (j=0; j<k; j++)
     {
        //B(:,j) = max( B(:,j) + Mat1(:,j) - B * Mat2(:,j), sEPS)
        //B(:,j) = max( B(:,j) + - B * Mat2(:,j) + Mat1(:,j), sEPS)
        CUBLASERR(cublasSgemv(handle1, CUBLAS_OP_N, n, k, &minusOne, B, n,  &Mat2[j*k], 1, &one, &Mat1[j*n], 1));
        CUBLASERR(cublasSaxpy(handle1,n, &one, &Mat1[j*n], 1, &B[j*n], 1));
        shalfwave_cuda(n, &B[j*n]);
     }

     //Stream 1 signals stream 2 so it can start working
     CUDAERR(cudaEventRecord(event2, stream1));
        
     //Stream2 waiting for event2 to continue executing 
     CUDAERR(cudaStreamWaitEvent(stream2, event2, 0));

     //Mat3 = A * B
     CUBLASERR(cublasSgemm(handle2, CUBLAS_OP_N, CUBLAS_OP_N, m, k, n, &one, A, m, B, n, &zero, Mat1, m));

     //Stream 2 signals stream 1 that has finished its part through event1
     CUDAERR(cudaEventRecord(event1, stream2));
		
     //Mat2 = B' * B
     CUBLASERR(cublasSgemm(handle1, CUBLAS_OP_T, CUBLAS_OP_N, k, k, n, &one, B, n, B, n, &zero, Mat2, k));
        
     //Stream 1 waits for stream 2 to finish its part
     CUDAERR(cudaStreamWaitEvent(stream1, event1, 0));

     for (j=0; j<k; j++)
     {
        //W(:,j) = max( W(:,j) * Mat2(j,j) + Mat3(:,j) - W * Mat2(:,j) , 0)
        CUBLASERR(cublasSgemv(handle1, CUBLAS_OP_N, m, k, &minusOne, W, m,  &Mat2[j*k], 1, &one, &Mat1[j*m], 1));

        //pointer mode change needed because Mat2(j,j) is in device
        CUBLASERR(cublasSetPointerMode(handle1, CUBLAS_POINTER_MODE_DEVICE));        
        CUBLASERR(cublasSaxpy(handle1, m, &Mat2[j+j*k], &W[j*m], 1, &Mat1[j*m], 1));

        CUBLASERR(cublasSetPointerMode(handle1, CUBLAS_POINTER_MODE_HOST));
        CUBLASERR(cublasScopy(handle1, m, &Mat1[j*m], 1, &W[j*m], 1));
        shalfwave_cuda(m, &W[j*m]);
            
        //W(:,j) = W(:,j) / norm(W(:,j));
        CUBLASERR(cublasSnrm2(handle1, m, &W[j*m], 1, &norm));
        norm = 1.0f/norm;
        CUBLASERR(cublasSscal(handle1, m, &norm, &W[j*m], 1));
     }
     //Stream 1 signals stream 2 so it can start working
     CUDAERR(cudaEventRecord(event2, stream1));
  }
    
  //strans(n ,k, B, H);
  CUBLASERR(cublasSgeam(handle1, CUBLAS_OP_T, CUBLAS_OP_N, k, n, &one, B, n, &zero,H, k, H, k));
    
  CUBLASERR(cublasDestroy(handle1));
  CUBLASERR(cublasDestroy(handle2));

  CUDAERR(cudaStreamDestroy(stream1));
  CUDAERR(cudaStreamDestroy(stream2)); 

  CUDAERR(cudaEventDestroy(event1));
  CUDAERR(cudaEventDestroy(event2)); 
  
  CUDAERR(cudaFree(B));
  CUDAERR(cudaFree(Mat1));
  CUDAERR(cudaFree(Mat2));
    
  return 0;
}
